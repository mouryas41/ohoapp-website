<?php
include('config.php');
include('notification_common.php');

$order_user_id = (isset($_POST['order_user_id'])) ? $_POST['order_user_id'] :'';
$order_key = (isset($_POST['order_key'])) ? $_POST['order_key'] :'';

// $ordersRef = $database->getReference('OHOUserAppDatabase/orders/'.$order_user_id)->getChild($order_key);
// $orderData = $ordersRef->getvalue();
// echo "<Pre>";
// print_r($orderData);

if(!empty($order_user_id) && !empty($order_key)){
	//Set move_status as 1 in OHOUserAppDatabase Table	
	$database->getReference('OHOUserAppDatabase/orders/'.$order_user_id.'/'.$order_key)->getChild('driver_accept_status')->set(0);

	//Set status as 1 in OHOCronData Table	
	$database->getReference('OHOCronData/'.$order_user_id)->getChild('status')->set(0);

	echo json_encode(array('message'=>'Rejected by driver.', 'status'=>'1'));
}
?>
