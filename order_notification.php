<?php
include('config.php');
include('notification_common.php');

$ids = $_POST['user_ids'];
$dts = $_POST['device_token'];
$title = $_POST['title'];
$message = $_POST['message'];
$ids  = str_replace(array("\r","\n",'\r','\n', ' '), '',$ids);
$dts  = str_replace(array("\r","\n",'\r','\n', ' '), '',$dts);
$dtarray = explode(',',trim($dts));
$idsarray = explode(',',trim($ids));
$dtsa = array_unique($dtarray);
$idsa = array_unique($idsarray);
$data = array_combine($idsa, $dtsa);
foreach($data as $key => $value){	
	$device_token = $value;
	send_notification($device_token, $title, $message);
	
	//Save notification
	if(trim($key) != ''){
	    $notification = [
	        "title" => $title,
	        "message" => $message
	    ];
		$database->getReference('OHONotification/'.$key)->push($notification);
	}
}

/*$dt = $_POST['device_token'];
$title = $_POST['title'];
$message = $_POST['message'];
$dtarray = explode(',',trim($dt));
$data = array_unique($dtarray);
foreach($data as $key){
	$device_token = $key;
	send_notification($device_token, $title, $message);
}*/
?>