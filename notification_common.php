<?php
function send_notification ($regid, $title, $msgg, $check = null)
{
    $ohoServerKey = 'AAAAJD-aLi4:APA91bGRKPGgHIs3AulWufq7ZeP25BqbSkuGtlHufOsqcNc0bXL4YM5CVOmf1w7J-PLH_9O9xGhya_2WrGSp4F5qe4iwC4IqZDpcF3rTV88GAnHeVuDt5eI79DgZef7H1tzoAlOLyk5L';
	
    //define('API_ACCESS_KEY', $ohoServerKey);
	if (!defined('API_ACCESS_KEY')) define('API_ACCESS_KEY', $ohoServerKey);

    $registrationIds        = $regid;
    $title                  = $title;
    $msg                    = $msgg;

    //prep the bundle
    $msg    =   array(
                    'body'  => $msg,
                    'title' => $title
                );
            
    $fields =   array(
                    'to'    => $registrationIds, 
                    'data'  => $msg
                );
                
    $headers =  array(
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                );
    
    #Send Reponse To FireBase Server    
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
    $result = curl_exec($ch );
    curl_close( $ch );
    if($check == null){
		echo $result;  
	}  
}
?>