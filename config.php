<?php
require __DIR__.'/vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

$serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/ohoo-114f5-firebase-adminsdk-e24r4-bf723a32fa.json');
$firebase = (new Factory)
	->withServiceAccount($serviceAccount)
	->withDatabaseUri('https://ohoo-114f5.firebaseio.com/')
	->create();
$database = $firebase->getDatabase();


//App URL
define('site_url',  "http://knickglobal.co.in/ohoapp/");


function get($dbname, int $userID = NULL){    
   if (empty($userID) || !isset($userID)) { return FALSE; }
   if ($this->database->getReference($dbname)->getSnapshot()->hasChild($userID)){
	   return $this->database->getReference($dbname)->getChild($userID)->getValue();
   } else {
	   return FALSE;
   }
}

function insert($dbname, array $data) {
   if (empty($data) || !isset($data)) { return FALSE; }
   foreach ($data as $key => $value){
	   $this->database->getReference()->getChild($dbname)->getChild($key)->set($value);
   }
   return TRUE;
}

function delete($dbname, int $userID) {
   if (empty($userID) || !isset($userID)) { return FALSE; }
   if ($this->database->getReference($dbname)->getSnapshot()->hasChild($userID)){
	   $this->database->getReference($dbname)->getChild($userID)->remove();
	   return TRUE;
   } else {
	   return FALSE;
   }
}