<?php
include('config.php');
include('notification_common.php');

function circle_distance($lat1, $lon1, $lat2, $lon2){
  $rad = M_PI / 180;
  return acos(sin($lat2*$rad) * sin($lat1*$rad) + cos($lat2*$rad) * cos($lat1*$rad) * cos($lon2*$rad - $lon1*$rad)) * 6371;// Kilometers
}

$title = (isset($_POST['title'])) ? $_POST['title'] :'';
$message = (isset($_POST['message'])) ? $_POST['message'] :'';
$lat = (is_numeric($_POST['lat'])) ? $_POST['lat'] : 0;
$long = (is_numeric($_POST['long'])) ? $_POST['long'] : 0;
$order_user_id = (isset($_POST['order_user_id'])) ? $_POST['order_user_id'] :'';
$order_key = (isset($_POST['order_key'])) ? $_POST['order_key'] :'';

// when drivers not accept first time
// So save this record for cron job purpose sending notification to all drivers
$createTbl = $database->getReference('OHOCronData')->getChild($order_user_id)
            ->set([
				'order_key' => $order_key,
                'farmar_latitude' => $lat,
                'farmar_longitude' => $long,
				'status' => 0
            ]);

$token = array();
$reference = $database->getReference('OHODriverAppDatabase');
$data = $reference->getvalue();
foreach($data['users'] as $key => $value){
	$latitude = $value['latitude'];
	$longitude =  $value['longitude'];
	$deviceToken =  $value['device_token'];
	
	//check distance between two location
	//if less then 20 then send notification
	if(circle_distance($lat,$long,$latitude,$longitude) <= 20 ){
		$token[] = $deviceToken;
	}
}

if(!empty($token)){
	foreach($token as $key){
		$device_token = $key;
		send_notification($device_token, $title, $message, 'driver');
	}
	echo json_encode(array('message'=>'Notification Sent', 'status'=>'1'));
}else{
	echo json_encode(array('message'=>'Not found near by any driver', 'status'=>'0'));
}
?>
