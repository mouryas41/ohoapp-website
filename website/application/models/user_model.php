<?php

class User_model extends CI_Model{

	function __construct(){	
        parent::__construct();
    }

    function record_count() {

        $database = $this->firebase->init();
		$users = $database->getReference('OHOUserAppDatabase/users')->getValue();
        return count($users);
    }

    
    function fetch_records() {

        $database = $this->firebase->init();
        $users = $database->getReference('OHOUserAppDatabase/users')->getValue();        
        if (count($users) > 0) {
            return $users;
        }	
        	
        // if (count($users) > 0) {
        //     foreach ($users as $key => $row) {
        //         $data[] = $row;
        //     }
        //     return $data;
        // }
        return false;
    }


    function fetch_user_by_id($uid){

		$database = $this->firebase->init();
		$users = $database->getReference('OHOUserAppDatabase/users')->getChild($uid)->getValue();
        return $users;
    }
	
	
}