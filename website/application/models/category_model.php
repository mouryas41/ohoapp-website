<?php

class Category_model extends CI_Model{

    function __construct(){
        parent::__construct();
    }


    function test()
    {
        //var storage = firebase.storage();
    }

    function fetch_categories(){

        $database = $this->firebase->init();
		$myads = $database->getReference('FruitsAndVegetables')->getValue();
		
        if (!empty($myads) && count($myads) > 0){
            return $myads;
        }
        return false;
    }


    function fetch_sub_categories(){

        $database = $this->firebase->init();
        $subcategories = $database->getReference('FruitsAndVegetables')->getValue();
        
        $subcategory = array();
        if (!empty($subcategories) && count($subcategories) > 0){
            foreach($subcategories as $category => $subcategoryArray){ 

                if(!empty($subcategoryArray)){
                    $data = array();
                    foreach($subcategoryArray as $val){ 
                        if(!empty($val)){          
                            $data['category'] = !empty($val['category']) ? $val['category']:'';
                            $data['image'] = !empty($val['image']) ? $val['image']:'';
                            $data['title'] = !empty($val['title']) ? $val['title']:'';
                            $data['id'] = !empty($val['id']) ? $val['id']:'';   
                            array_push($subcategory, $data);                 
                        }                
                    }
                }
            }
            // echo "<pre>";
            // print_r($subcategory);
            // echo "<pre>";
            // die;
            return $subcategory;
        }              
        return false;
    }


    function get_subcategory_info($category, $id) {

		$database = $this->firebase->init();
        $subcategories = $database->getReference('FruitsAndVegetables/'.$category.'/'.$id)->getValue();
        return $subcategories;
    }


    //check last exist category key
    function check_last_subcategory($category) {

        $database = $this->firebase->init();                   
        $categories = $database->getReference('FruitsAndVegetables/'.$category)->getValue();        
        $lastkey = !empty($categories) ? end($categories):'';
        $key = !empty($lastkey) ? key($categories)+1 :'1';
        return $key;
    }
    
    function add_cateogry($category)
    {        
        $database = $this->firebase->init();           
        $newData = array(
            'id' => 1,      
            'title' => $category,
            'category' => $category,
            'image' => '',                         
        );
        $newsubcategory = $database->getReference('FruitsAndVegetables/'.$category.'/1')->set($newData);
    }

    
    function delete_cateogry($category) 
    {
        $database = $this->firebase->init();        
        $deleteSubcategory = $database->getReference('FruitsAndVegetables/'.$category)->remove();
    }



    function add_sub_cateogry($category, $new_catid, $data)
    {        
        $database = $this->firebase->init();   
        $newsubcategory = $database->getReference('FruitsAndVegetables/'.$category.'/'.$new_catid)->set($data);
    }

    function update_sub_cateogry($category, $subcategory_id, $data)
    {               
        $database = $this->firebase->init();   
        $newsubcategory = $database->getReference('FruitsAndVegetables/'.$category.'/'.$subcategory_id)->update($data);
    }


    function delete_sub_cateogry($category, $subcategory_id) 
    {
        $database = $this->firebase->init();        
        $deleteSubcategory = $database->getReference('FruitsAndVegetables/'.$category.'/'.$subcategory_id)->remove();
    }

} 