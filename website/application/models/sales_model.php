<?php
class Sales_model extends CI_Model{

    function __construct(){

        parent::__construct();
    }

    function record_count(){

        $database = $this->firebase->init();
		$orders = $database->getReference('Orders')->getValue();
        return count($orders);
    }
 	
	
	function get_machine_user($uid){
		
        $database = $this->firebase->init();
		$users = $database->getReference('Users/'.$uid)->getValue();
		return $users['user_name'];
    }
	
	
	function get_user($uid){
		
        $database = $this->firebase->init();
		$users = $database->getReference('OHOUserAppDatabase/users/'.$uid)->getValue();
		return $users['user_name'];
    }
 	
	
	function fetch_machine_sales_data(){

        $database = $this->firebase->init();
		$orders = $database->getReference('Orders')->getValue();
		
		$data = [];	
        if (count($orders) > 0) {      
			if(!empty($orders)){
				foreach($orders as $key => $value){ 	
					$temp = [];		
					$temp['customerId'] = $this->get_machine_user($key);	
					foreach($value as $ckey => $cvalue){						
						if(trim($cvalue['order_status']) == 'delivered'){
							$temp['cartkey'] = $ckey;
							$temp['total_amount'] = $cvalue['total_amount'];
							$temp['order_status'] = $cvalue['order_status'];
							$temp['order_date'] = date("d-m-Y", strtotime($cvalue['order_date']));	
							foreach($cvalue['cartItems'] as $ky => $item){								
								$temp['cartid'] = !empty($item['id']) ? $item['id']:'';								
								$temp['title'] = !empty($item['title']) ? $item['title']:'';
								$temp['machine'] = !empty($item['machine']) ? $item['machine']:'';
								$temp['machine_id'] = !empty($item['machine_id']) ? $item['machine_id']:'';
								$temp['machine_owner_id'] = !empty($item['machine_owner_id']) ? $item['machine_owner_id']:'';
								$temp['description'] = !empty($item['description']) ? $item['description']:'';
								$temp['price'] = !empty($item['price']) ? $item['price']:'';
								$temp['quentity'] = !empty($item['unit_quantity']) ? $item['unit_quantity']:'';
								$temp['type'] = !empty($item['type_of_machine']) ? $item['type_of_machine']:'';
								$temp['image'] = !empty($item['images'][0]) ? $item['images'][0]:'';
								$data[]=$temp;										
							}	
						}					
					}						
				}	
			}	
            return $data;
        }
        return false;
    }
	

    function fetch_machine_sales_by_date($sdate,$edate){

        $database = $this->firebase->init();
		$orders = $database->getReference('Orders')->getValue();
		
		$data = [];	
		if (count($orders) > 0){  
			foreach($orders as $key => $value){ 	
				$temp = [];		
				$temp['customerId'] = $this->get_machine_user($key);
				foreach($value as $ckey => $cvalue){
					if(trim($cvalue['order_status']) == 'delivered'){
						$temp['cartkey'] = $ckey;	
						$temp['total_amount'] = $cvalue['total_amount'];
						$temp['order_status'] = $cvalue['order_status'];					
						$order_date = date("d-m-Y", strtotime($cvalue['order_date']));  					
						
						if(strtotime($sdate) <= strtotime($order_date) && strtotime($edate) >= strtotime($order_date)){										
							$temp['order_date'] = date("d-m-Y", strtotime($cvalue['order_date']));							
							foreach($cvalue['cartItems'] as $ky => $item){
								$temp['cartid'] = !empty($item['id']) ? $item['id']:'';
								$temp['title'] = !empty($item['title']) ? $item['title']:'';
								$temp['machine'] = !empty($item['machine']) ? $item['machine']:'';
								$temp['machine_id'] = !empty($item['machine_id']) ? $item['machine_id']:'';
								$temp['machine_owner_id'] = !empty($item['machine_owner_id']) ? $item['machine_owner_id']:'';
								$temp['description'] = !empty($item['description']) ? $item['description']:'';
								$temp['price'] = !empty($item['price']) ? $item['price']:'';
								$temp['quentity'] = !empty($item['unit_quantity']) ? $item['unit_quantity']:'';
								$temp['type'] = !empty($item['type_of_machine']) ? $item['type_of_machine']:'';
								$temp['image'] = !empty($item['images'][0]) ? $item['images'][0]:'';
								$data[]=$temp;										
							}						
						}
					}
				}						
			}
			return $data;
		}
        return false;		
	}
	

	function fetch_vendor_info($vendorId){		
		$database = $this->firebase->init();
		$vendorInfo = $database->getReference('Users/'.$vendorId)->getValue();
		return $vendorInfo;		
	}

	
	
	function fetch_veg_sales_data(){

        $database = $this->firebase->init();
		$orders = $database->getReference('OHOUserAppDatabase/orders')->getValue();
		
		$data = [];	
        if (count($orders) > 0) {      
			if(!empty($orders)){
				foreach($orders as $key => $value){ 	
					$temp = [];		
					$temp['customerId'] = $this->get_user($key);
					foreach($value as $ckey => $cvalue){
						if(trim($cvalue['order_status']) == 'delivered'){
							$temp['cartkey'] = $ckey;	
							$temp['total_amount'] = $cvalue['total_amount'];
							$temp['order_status'] = $cvalue['order_status'];
							$temp['order_date'] = date("d-m-Y", strtotime($cvalue['order_date']));	
							foreach($cvalue['cartItems'] as $ky => $item){														
								$temp['cartid'] = !empty($item['id']) ? $item['id']:'';
								$temp['title'] = !empty($item['title']) ? $item['title']:'';
								$temp['product_id'] = !empty($item['product_id']) ? $item['product_id']:'';	
								$temp['product_owner_info'] = !empty($item['product_owner_id']) ? $this->fetch_vendor_info($item['product_owner_id']):'';														
								$temp['price'] = !empty($item['price']) ? $item['price']:'';
								$temp['quentity'] = !empty($item['unit_quantity']) ? $item['unit_quantity']:'';
								$temp['type'] = !empty($item['category']) ? $item['category']:'';
								$temp['image'] = !empty($item['image']) ? $item['image']:'';								
								$data[]=$temp;										
							}
						}								
					}						
				}	
			}					
            return $data;
        }
        return false;
    }
	
	
	function fetch_veg_sales_by_date($sdate,$edate){

        $database = $this->firebase->init();
		$orders = $database->getReference('OHOUserAppDatabase/orders')->getValue();
		
		$data = [];	
		if (count($orders) > 0){  
			foreach($orders as $key => $value){ 	
				$temp = [];		
				$temp['customerId'] = $this->get_user($key);	
				foreach($value as $ckey => $cvalue){
					if(trim($cvalue['order_status']) == 'delivered'){
						$temp['cartkey'] = $ckey;
						$temp['total_amount'] = $cvalue['total_amount'];
						$temp['order_status'] = $cvalue['order_status'];
						$order_date = date("d-m-Y", strtotime($cvalue['order_date']));  					
						
						if(strtotime($sdate) <= strtotime($order_date) && strtotime($edate) >= strtotime($order_date)){											
							$temp['order_date'] = date("d-m-Y", strtotime($cvalue['order_date']));							
							foreach($cvalue['cartItems'] as $ky => $item){
								$temp['cartid'] = !empty($item['id']) ? $item['id']:'';
								$temp['title'] = !empty($item['title']) ? $item['title']:'';
								$temp['product_id'] = !empty($item['product_id']) ? $item['product_id']:'';	
								$temp['product_owner_info'] = !empty($item['product_owner_id']) ? $this->fetch_vendor_info($item['product_owner_id']):'';						
								$temp['price'] = !empty($item['price']) ? $item['price']:'';
								$temp['quentity'] = !empty($item['unit_quantity']) ? $item['unit_quantity']:'';
								$temp['type'] = !empty($item['category']) ? $item['category']:'';
								$temp['image'] = !empty($item['image']) ? $item['image']:'';
								$data[]=$temp;											
							}						
						}
					}
				}						
			}
			return $data;
		}
        return false;		
	}

} 