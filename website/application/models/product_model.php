<?php
class Product_model extends CI_Model{

    function __construct(){

        parent::__construct();
    }

    function products_count(){

        $database = $this->firebase->init();
		$orders = $database->getReference('Orders')->getValue();
        return count($orders);
	}
	

	function fetch_vendor_info($vendorId){		
		$database = $this->firebase->init();
		$vendorInfo = $database->getReference('Users/'.$vendorId)->getValue();
		return $vendorInfo;		
	}
 	

	function fetch_products(){

        $database = $this->firebase->init();
		$fetchProducts = $database->getReference('MyFruitsAndVegetables')->getValue();
	
		$data = [];	
        if (count($fetchProducts) > 0) {  
			foreach($fetchProducts as $vendor_id => $productArray){ 
				if(empty($productArray)) {
					continue;
				}  
				
				$temp = [];		
				$vendor_id = trim(trim('"'.$vendor_id.'"','"'));	
				foreach($productArray as $product_key => $pvalue){
					$temp['vendorId'] = $vendor_id;
					$temp['vendor_name'] = $this->fetch_vendor_info($vendor_id);					
					$temp['product_key'] = $product_key;
					$temp['product_id'] = !empty($pvalue['id']) ? $pvalue['id']:'';
					$temp['category'] = !empty($pvalue['category']) ? $pvalue['category']:'';
					$temp['price'] = !empty($pvalue['price']) ? $pvalue['price']:'';					
					$temp['quantity'] = !empty($pvalue['quantity']) ? $pvalue['quantity']:'';
					$temp['title'] = !empty($pvalue['title']) ? $pvalue['title']:'';	
					$temp['image'] = !empty($pvalue['image']) ? $pvalue['image']:'';															
					$data[]=$temp;									
				}						
			}			
            return $data;
        }
		return false;

	}


	function fetch_vendor_orders_by_date($sdate, $edate){

		$database = $this->firebase->init();
		$fetchOrders = $database->getReference('Orders')->getValue();
	
		$data = [];	
        if (count($fetchOrders) > 0) {  
			foreach($fetchOrders as $vendor_id => $orderArray){ 
				if(empty($orderArray)) {
					continue;
				}  

				$temp = [];		
				$vendor_id = trim(trim('"'.$vendor_id.'"','"'));					
				foreach($orderArray as $order_key => $cvalue){
					$order_date = date("d-m-Y", strtotime($cvalue['order_date']));
					if(strtotime($sdate) <= strtotime($order_date) && strtotime($edate) >= strtotime($order_date)){							
						$temp['vendorId'] = $vendor_id;
						$temp['order_key'] = $order_key;
						$temp['order_id'] = !empty($cvalue['order_id']) ? $cvalue['order_id']:'';
						$temp['order_status'] = !empty($cvalue['order_status']) ? $cvalue['order_status']:'';
						$temp['total_amount'] = !empty($cvalue['total_amount']) ? $cvalue['total_amount']:'';					
						$temp['delivery_amount'] = !empty($cvalue['delivery_amount']) ? $cvalue['delivery_amount']:'';
						$temp['order_date'] = date("d-m-Y", strtotime($cvalue['order_date']));
						$temp['user_name'] = !empty($cvalue['userDetails']['user_name']) ? $cvalue['userDetails']['user_name']:'';
						$data[]=$temp;		
					}							
				}						
			}						
            return $data;
        }
		return false;					
	}


	function view_vendor_order_detail($vendorId, $orderId){

        $database = $this->firebase->init();
		$fetchOrder = $database->getReference('Orders/'.$vendorId.'/'.$orderId)->getValue();		
		
		$data = [];	
        if (count($fetchOrder) > 0) {  					
			$data['order_id'] = !empty($fetchOrder['order_id']) ? $fetchOrder['order_id']:'';				
			$data['order_status'] = !empty($fetchOrder['order_status']) ? $fetchOrder['order_status']:'';
			$data['total_amount'] = !empty($fetchOrder['total_amount']) ? $fetchOrder['total_amount']:'';					
			$data['delivery_amount'] = !empty($fetchOrder['delivery_amount']) ? $fetchOrder['delivery_amount']:'';
			$data['order_date'] = !empty($fetchOrder['order_date']) ? $fetchOrder['order_date']:'';	
			$data['order_time'] = !empty($fetchOrder['order_time']) ? $fetchOrder['order_time']:'';
			$data['cartItems'] = !empty($fetchOrder['cartItems']) ? $fetchOrder['cartItems']:'';
			$data['userDetails'] = !empty($fetchOrder['userDetails']) ? $fetchOrder['userDetails']:'';						
            return $data;
        }
		return false;
	}






	function fetch_customer_orders(){

        $database = $this->firebase->init();
		$fetchOrders = $database->getReference('OHOUserAppDatabase/orders')->getValue();
	
		$data = [];	
        if (count($fetchOrders) > 0) {  
			foreach($fetchOrders as $customer_id => $orderArray){ 
				if(empty($orderArray)) {
					continue;
				}  

				$temp = [];		
				$customer_id = trim(trim('"'.$customer_id.'"','"'));	
				foreach($orderArray as $order_key => $cvalue){
					$temp['customerId'] = $customer_id;
					$temp['order_key'] = $order_key;
					$temp['order_id'] = !empty($cvalue['order_id']) ? $cvalue['order_id']:'';
					$temp['order_status'] = !empty($cvalue['order_status']) ? $cvalue['order_status']:'';
					$temp['total_amount'] = !empty($cvalue['total_amount']) ? $cvalue['total_amount']:'';					
					$temp['delivery_amount'] = !empty($cvalue['delivery_amount']) ? $cvalue['delivery_amount']:'';
					$temp['order_date'] = date("d-m-Y", strtotime($cvalue['order_date']));						
					$temp['user_name'] = !empty($cvalue['userDetails']['user_name']) ? $cvalue['userDetails']['user_name']:'';
					$data[]=$temp;									
				}						
			}			
            return $data;
        }
		return false;

	}


	function fetch_customer_orders_by_date($sdate, $edate){

		$database = $this->firebase->init();
		$fetchOrders = $database->getReference('OHOUserAppDatabase/orders')->getValue();
	
		$data = [];	
        if (count($fetchOrders) > 0) {  
			foreach($fetchOrders as $customer_id => $orderArray){ 
				if(empty($orderArray)) {
					continue;
				}  

				$temp = [];		
				$customer_id = trim(trim('"'.$customer_id.'"','"'));					
				foreach($orderArray as $order_key => $cvalue){
					$order_date = date("d-m-Y", strtotime($cvalue['order_date']));
					if(strtotime($sdate) <= strtotime($order_date) && strtotime($edate) >= strtotime($order_date)){							
						$temp['customerId'] = $customer_id;
						$temp['order_key'] = $order_key;
						$temp['order_id'] = !empty($cvalue['order_id']) ? $cvalue['order_id']:'';
						$temp['order_status'] = !empty($cvalue['order_status']) ? $cvalue['order_status']:'';
						$temp['total_amount'] = !empty($cvalue['total_amount']) ? $cvalue['total_amount']:'';					
						$temp['delivery_amount'] = !empty($cvalue['delivery_amount']) ? $cvalue['delivery_amount']:'';
						$temp['order_date'] = date("d-m-Y", strtotime($cvalue['order_date']));
						$temp['user_name'] = !empty($cvalue['userDetails']['user_name']) ? $cvalue['userDetails']['user_name']:'';
						$data[]=$temp;		
					}							
				}						
			}						
            return $data;
        }
		return false;					
	}


	function fetch_driver_info($driverId){		
		$database = $this->firebase->init();
		$driverInfo = $database->getReference('OHODriverAppDatabase/users/'.$driverId)->getValue();
		return $driverInfo;		
	}


	function view_customer_order_detail($customerId, $orderId){

        $database = $this->firebase->init();
		$fetchOrder = $database->getReference('OHOUserAppDatabase/orders/'.$customerId.'/'.$orderId)->getValue();		
		
		$data = [];	
        if (count($fetchOrder) > 0) {  					
			$data['order_id'] = !empty($fetchOrder['order_id']) ? $fetchOrder['order_id']:'';				
			$data['order_status'] = !empty($fetchOrder['order_status']) ? $fetchOrder['order_status']:'';
			$data['total_amount'] = !empty($fetchOrder['total_amount']) ? $fetchOrder['total_amount']:'';					
			$data['delivery_amount'] = !empty($fetchOrder['delivery_amount']) ? $fetchOrder['delivery_amount']:'';
			$data['order_date'] = !empty($fetchOrder['order_date']) ? $fetchOrder['order_date']:'';	
			$data['order_time'] = !empty($fetchOrder['order_time']) ? $fetchOrder['order_time']:'';
			$data['cartItems'] = !empty($fetchOrder['cartItems']) ? $fetchOrder['cartItems']:'';
			$data['userDetails'] = !empty($fetchOrder['userDetails']) ? $fetchOrder['userDetails']:'';
			
			if($fetchOrder['driver_accept_status'] == 1){
				$data['driver_user_id'] = !empty($fetchOrder['driver_user_id']) ? $fetchOrder['driver_user_id']:'';
				$data['driver_info'] = !empty($data['driver_user_id']) ? $this->fetch_driver_info($data['driver_user_id']):'';				
			}			
			$data['order_delivery_date'] = !empty($fetchOrder['order_delivery_date']) ? $fetchOrder['order_delivery_date']:'';
			$data['order_delivery_time'] = !empty($fetchOrder['order_delivery_time']) ? $fetchOrder['order_delivery_time']:'';	
            return $data;
        }
		return false;
	}


	function fetch_vendors(){

        $database = $this->firebase->init();
		$farmers = $database->getReference('Users')->getValue(); 
		
        if (count($farmers) > 0) {
            return $farmers;
        }
        return false;
    }


	/*function saverecords($vendor,$category,$title,$quantity,$price)
	{
		$database = $this->firebase->init();
		$checkvendor = $database->getReference('MyFruitsAndVegetables/'.$vendor)->getValue();

        $blankInt = '0';
		if(!empty($checkvendor)){
			$count = count($checkvendor);
			$count = $count + 1;
			$nid = strval($count);
			$newData = array(				  				
				'category' => $category,				
				'device_token' => '',
				'id' => $nid,    
				'image' => '',    
				'latitude' => (int)$blankInt,
				'longitude' => (int)$blankInt, 
				'price' => $price,
				'product_owner_id' => $vendor,
				'quantity' => (int)$quantity,  
				'row_id' => '',
				'title' => $title,
				'status' => 'publish',
			);
			$newProductkey = $database->getReference('MyFruitsAndVegetables/'.$vendor)->push($newData)->getKey();
			$checkProduct = $database->getReference('MyFruitsAndVegetables/'.$vendor.'/'.$newProductkey)->getChild('row_id')->getValue();			
			if(trim($checkProduct) == ''){
				$database->getReference('MyFruitsAndVegetables/'.$vendor.'/'.$newProductkey)->getChild('row_id')->set($newProductkey);
			}
		}else{	
			//echo 'add new product 1'; 		
			$newData = array(				  				
				'category' => $category,				
				'device_token' => '',
				'id' => '1',    
				'image' => '',    
				'latitude' => (int)$blankInt,
				'longitude' => (int)$blankInt, 
				'price' => $price,
				'product_owner_id' => $vendor,
				'quantity' => (int)$quantity,
				'row_id' => '',
				'title' => $title,
				'status' => 'publish',
			);
			$newProductkey = $database->getReference('MyFruitsAndVegetables/'.$vendor)->push($newData)->getKey();
			$checkProduct = $database->getReference('MyFruitsAndVegetables/'.$vendor.'/'.$newProductkey)->getChild('row_id')->getValue();			
			if(trim($checkProduct) == ''){
				$database->getReference('MyFruitsAndVegetables/'.$vendor.'/'.$newProductkey)->getChild('row_id')->set($newProductkey);
			}
		}		

	}*/
	
	

	function saverecords($vendor,$category,$title,$quantity,$price)
	{
		$database = $this->firebase->init();
		$checkvendor = $database->getReference('MyFruitsAndVegetables/'.$vendor)->getValue();

        $blankInt = '0';
		$title = trim(ucfirst($title));
		$category = trim(ucfirst($category));
		if(!empty($checkvendor)){
				
			//Check duplicate products 	
			$productId = array();		
			foreach($checkvendor as $productKey => $productArray){			
				$prev_category = trim($productArray['category']); 
				$prev_title = trim($productArray['title']);				
				if($category == $prev_category && $title == $prev_title)
				{	
					$productId[] = $productKey;	 			
				}			
			}	
			 
			//if found duplicate product then update else added new product
			if(!empty($productId))
			{		
				// echo "update product quantity and price";			
				$product_Id = !empty($productId[0]) ? $productId[0]:'';		
				
				$againCheckProduct = $database->getReference('MyFruitsAndVegetables/'.$vendor.'/'.$product_Id)->getValue();
				if(!empty($againCheckProduct)){	
					
					$prevQuentity = $againCheckProduct['quantity'];
					$newQuentity = (int)$prevQuentity+$quantity;										
					$newPrice = $price;
					
					// update price
					$database->getReference('MyFruitsAndVegetables/'.$vendor.'/'.$product_Id)->getChild('price')->set($newPrice);
					// update quentity
					$database->getReference('MyFruitsAndVegetables/'.$vendor.'/'.$product_Id)->getChild('quantity')->set($newQuentity); 
				} 
			}
			else
			{
				//echo "add continue new product";
				$count = count($checkvendor);
				$count = $count + 1;
				$nid = strval($count);
				$newData = array(				  				
					'category' => $category,				
					'device_token' => '',
					'id' => $nid,    
					'image' => '',    
					'latitude' => (int)$blankInt,
					'longitude' => (int)$blankInt, 
					'price' => $price,
					'product_owner_id' => $vendor,
					'quantity' => (int)$quantity,  
					'row_id' => '',
					'title' => $title,
					'status' => 'publish',
				);
				$newProductkey = $database->getReference('MyFruitsAndVegetables/'.$vendor)->push($newData)->getKey();
				$checkProduct = $database->getReference('MyFruitsAndVegetables/'.$vendor.'/'.$newProductkey)->getChild('row_id')->getValue();			
				if(trim($checkProduct) == ''){
					$database->getReference('MyFruitsAndVegetables/'.$vendor.'/'.$newProductkey)->getChild('row_id')->set($newProductkey);
				}
			}

		}else{	
					
			//echo 'add new product'; 		
			$newData = array(				  				
				'category' => $category,				
				'device_token' => '',
				'id' => '1',    
				'image' => '',    
				'latitude' => (int)$blankInt,
				'longitude' => (int)$blankInt, 
				'price' => $price,
				'product_owner_id' => $vendor,
				'quantity' => (int)$quantity,
				'row_id' => '',
				'title' => $title,
				'status' => 'publish',
			);
			$newProductkey = $database->getReference('MyFruitsAndVegetables/'.$vendor)->push($newData)->getKey();
			$checkProduct = $database->getReference('MyFruitsAndVegetables/'.$vendor.'/'.$newProductkey)->getChild('row_id')->getValue();			
			if(trim($checkProduct) == ''){
				$database->getReference('MyFruitsAndVegetables/'.$vendor.'/'.$newProductkey)->getChild('row_id')->set($newProductkey);
			}
		}		

	}

	

} 