<?php

class Driver_model extends CI_Model{

    function __construct(){

        parent::__construct();
    }

    function record_count() {

        return $this->db->count_all("master_qb");
    }

    function fetch_question($limit, $start) {

        $this->db->limit($limit, $start);
        $query = $this->db->get("master_qb");

        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {

                $data[] = $row;
            }

            return $data;
        }

        return false;
    }

    function fetch_question_by_id($qn_id) {

        $query = $this->db->get_where('master_qb',array('id'=>$qn_id));

        return $query->row();
    }

    function employee_existence($employee_id) {

        $query = $this->db->get_where('salary',array('employee_id'=>$employee_id));

        if($query->row() > 0){

            return true;
        }

        return false;
    }

    function save_question($data) {

        $this->db->insert('master_qb', $data);
    }

    function update_question_by_id($id,$data) {

        $this->db->where('id', $id);
        $this->db->update('master_qb', $data);
    }

    function update_salary_status($employee_id) {

        $this->db->where('employee_id', $employee_id);
        $this->db->update('employee', array('employee_salary'=>1));
    }

    function erase_question($id) {

        $this->db->delete('master_qb', array('id' => $id));
    }

} 