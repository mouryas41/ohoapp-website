<?php

class Farmer_model extends CI_Model{

    function __construct(){
        parent::__construct();
    }

    function record_count(){

	    $database = $this->firebase->init();
		$farmers = $database->getReference('Users')->getValue();
        return count($farmers);
    }

    function fetch_records(){

        $database = $this->firebase->init();
		$farmers = $database->getReference('Users')->getValue(); 
		
        if (count($farmers) > 0) {
            return $farmers;
        }
        return false;
    }
	
	
	function fetch_by_id($fid){

		$database = $this->firebase->init();
		$farmers = $database->getReference('Users')->getChild($fid)->getValue();
        return $farmers;
    }

	function delete_record($fid) {

        // $database = $this->firebase->init();
		// $farmers = $database->getReference('Users')->getChild($fid)->getValue();
    }
	
	
	/* function save_file($data) {
        $this->db->insert('master_files', $data);
    } */
	
	
	/* function update_file_by_id($id,$data) {
		
		if(!empty($data['audio_file'])){
			$query = $this->db->get_where('master_files',array('id'=>$id));
			$audio_file = $query->row()->audio_file;
			unlink('../files/'.$audio_file);
		}
        $this->db->where('id', $id);
        $this->db->update('master_files', $data);
    } */


    /* function erase_file($fid) {

		$query = $this->db->get_where('master_files',array('id'=>$fid));
		$audio_file = $query->row()->audio_file;
		unlink('../files/'.$audio_file);
        $this->db->delete('master_files', array('id' => $fid));
    } */

} 