<?php
class Order_model extends CI_Model{

    function __construct(){

        parent::__construct();
    }

    function vendor_orders_count(){

        $database = $this->firebase->init();
		$orders = $database->getReference('Orders')->getValue();
        return count($orders);
    }
 	

	function fetch_vendor_orders(){

        $database = $this->firebase->init();
		$fetchOrders = $database->getReference('Orders')->getValue();
	
		$data = [];	
        if (count($fetchOrders) > 0) {  
			foreach($fetchOrders as $vendor_id => $orderArray){ 
				if(empty($orderArray)) {
					continue;
				}  

				$temp = [];		
				$vendor_id = trim(trim('"'.$vendor_id.'"','"'));	
				foreach($orderArray as $order_key => $cvalue){
					$temp['vendorId'] = $vendor_id;
					$temp['order_key'] = $order_key;
					$temp['order_id'] = !empty($cvalue['order_id']) ? $cvalue['order_id']:'';
					$temp['order_status'] = !empty($cvalue['order_status']) ? $cvalue['order_status']:'';
					$temp['total_amount'] = !empty($cvalue['total_amount']) ? $cvalue['total_amount']:'';					
					$temp['delivery_amount'] = !empty($cvalue['delivery_amount']) ? $cvalue['delivery_amount']:'';
					$temp['order_date'] = date("d-m-Y", strtotime($cvalue['order_date']));						
					$temp['user_name'] = !empty($cvalue['userDetails']['user_name']) ? $cvalue['userDetails']['user_name']:'';
					$data[]=$temp;									
				}						
			}			
            return $data;
        }
		return false;

	}


	function fetch_vendor_orders_by_date($sdate, $edate){

		$database = $this->firebase->init();
		$fetchOrders = $database->getReference('Orders')->getValue();
	
		$data = [];	
        if (count($fetchOrders) > 0) {  
			foreach($fetchOrders as $vendor_id => $orderArray){ 
				if(empty($orderArray)) {
					continue;
				}  

				$temp = [];		
				$vendor_id = trim(trim('"'.$vendor_id.'"','"'));					
				foreach($orderArray as $order_key => $cvalue){
					$order_date = date("d-m-Y", strtotime($cvalue['order_date']));
					if(strtotime($sdate) <= strtotime($order_date) && strtotime($edate) >= strtotime($order_date)){							
						$temp['vendorId'] = $vendor_id;
						$temp['order_key'] = $order_key;
						$temp['order_id'] = !empty($cvalue['order_id']) ? $cvalue['order_id']:'';
						$temp['order_status'] = !empty($cvalue['order_status']) ? $cvalue['order_status']:'';
						$temp['total_amount'] = !empty($cvalue['total_amount']) ? $cvalue['total_amount']:'';					
						$temp['delivery_amount'] = !empty($cvalue['delivery_amount']) ? $cvalue['delivery_amount']:'';
						$temp['order_date'] = date("d-m-Y", strtotime($cvalue['order_date']));
						$temp['user_name'] = !empty($cvalue['userDetails']['user_name']) ? $cvalue['userDetails']['user_name']:'';
						$data[]=$temp;		
					}							
				}						
			}						
            return $data;
        }
		return false;					
	}


	function view_vendor_order_detail($vendorId, $orderId){

        $database = $this->firebase->init();
		$fetchOrder = $database->getReference('Orders/'.$vendorId.'/'.$orderId)->getValue();		
		
		$data = [];	
        if (count($fetchOrder) > 0) {  					
			$data['order_id'] = !empty($fetchOrder['order_id']) ? $fetchOrder['order_id']:'';				
			$data['order_status'] = !empty($fetchOrder['order_status']) ? $fetchOrder['order_status']:'';
			$data['total_amount'] = !empty($fetchOrder['total_amount']) ? $fetchOrder['total_amount']:'';					
			$data['delivery_amount'] = !empty($fetchOrder['delivery_amount']) ? $fetchOrder['delivery_amount']:'';
			$data['order_date'] = !empty($fetchOrder['order_date']) ? $fetchOrder['order_date']:'';	
			$data['order_time'] = !empty($fetchOrder['order_time']) ? $fetchOrder['order_time']:'';
			$data['cartItems'] = !empty($fetchOrder['cartItems']) ? $fetchOrder['cartItems']:'';
			$data['userDetails'] = !empty($fetchOrder['userDetails']) ? $fetchOrder['userDetails']:'';						
            return $data;
        }
		return false;
	}






	function fetch_customer_orders(){

        $database = $this->firebase->init();
		$fetchOrders = $database->getReference('OHOUserAppDatabase/orders')->getValue();
	
		$data = [];	
        if (count($fetchOrders) > 0) {  
			foreach($fetchOrders as $customer_id => $orderArray){ 
				if(empty($orderArray)) {
					continue;
				}  

				$temp = [];		
				$customer_id = trim(trim('"'.$customer_id.'"','"'));	
				foreach($orderArray as $order_key => $cvalue){
					$temp['customerId'] = $customer_id;
					$temp['order_key'] = $order_key;
					$temp['order_id'] = !empty($cvalue['order_id']) ? $cvalue['order_id']:'';
					$temp['order_status'] = !empty($cvalue['order_status']) ? $cvalue['order_status']:'';
					$temp['total_amount'] = !empty($cvalue['total_amount']) ? $cvalue['total_amount']:'';					
					$temp['delivery_amount'] = !empty($cvalue['delivery_amount']) ? $cvalue['delivery_amount']:'';
					$temp['order_date'] = date("d-m-Y", strtotime($cvalue['order_date']));						
					$temp['user_name'] = !empty($cvalue['userDetails']['user_name']) ? $cvalue['userDetails']['user_name']:'';
					$data[]=$temp;									
				}						
			}			
            return $data;
        }
		return false;

	}


	function fetch_customer_orders_by_date($sdate, $edate){

		$database = $this->firebase->init();
		$fetchOrders = $database->getReference('OHOUserAppDatabase/orders')->getValue();
	
		$data = [];	
        if (count($fetchOrders) > 0) {  
			foreach($fetchOrders as $customer_id => $orderArray){ 
				if(empty($orderArray)) {
					continue;
				}  

				$temp = [];		
				$customer_id = trim(trim('"'.$customer_id.'"','"'));					
				foreach($orderArray as $order_key => $cvalue){
					$order_date = date("d-m-Y", strtotime($cvalue['order_date']));
					if(strtotime($sdate) <= strtotime($order_date) && strtotime($edate) >= strtotime($order_date)){							
						$temp['customerId'] = $customer_id;
						$temp['order_key'] = $order_key;
						$temp['order_id'] = !empty($cvalue['order_id']) ? $cvalue['order_id']:'';
						$temp['order_status'] = !empty($cvalue['order_status']) ? $cvalue['order_status']:'';
						$temp['total_amount'] = !empty($cvalue['total_amount']) ? $cvalue['total_amount']:'';					
						$temp['delivery_amount'] = !empty($cvalue['delivery_amount']) ? $cvalue['delivery_amount']:'';
						$temp['order_date'] = date("d-m-Y", strtotime($cvalue['order_date']));
						$temp['user_name'] = !empty($cvalue['userDetails']['user_name']) ? $cvalue['userDetails']['user_name']:'';
						$data[]=$temp;		
					}							
				}						
			}						
            return $data;
        }
		return false;					
	}


	function fetch_driver_info($driverId){		
		$database = $this->firebase->init();
		$driverInfo = $database->getReference('OHODriverAppDatabase/users/'.$driverId)->getValue();
		return $driverInfo;		
	}


	function view_customer_order_detail($customerId, $orderId){

        $database = $this->firebase->init();
		$fetchOrder = $database->getReference('OHOUserAppDatabase/orders/'.$customerId.'/'.$orderId)->getValue();		
		
		$data = [];	
        if (count($fetchOrder) > 0) {  					
			$data['order_id'] = !empty($fetchOrder['order_id']) ? $fetchOrder['order_id']:'';				
			$data['order_status'] = !empty($fetchOrder['order_status']) ? $fetchOrder['order_status']:'';
			$data['total_amount'] = !empty($fetchOrder['total_amount']) ? $fetchOrder['total_amount']:'';					
			$data['delivery_amount'] = !empty($fetchOrder['delivery_amount']) ? $fetchOrder['delivery_amount']:'';
			$data['order_date'] = !empty($fetchOrder['order_date']) ? $fetchOrder['order_date']:'';	
			$data['order_time'] = !empty($fetchOrder['order_time']) ? $fetchOrder['order_time']:'';
			$data['cartItems'] = !empty($fetchOrder['cartItems']) ? $fetchOrder['cartItems']:'';
			$data['userDetails'] = !empty($fetchOrder['userDetails']) ? $fetchOrder['userDetails']:'';
			
			if($fetchOrder['driver_accept_status'] == 1){
				$data['driver_user_id'] = !empty($fetchOrder['driver_user_id']) ? $fetchOrder['driver_user_id']:'';
				$data['driver_info'] = !empty($data['driver_user_id']) ? $this->fetch_driver_info($data['driver_user_id']):'';				
			}			
			$data['order_delivery_date'] = !empty($fetchOrder['order_delivery_date']) ? $fetchOrder['order_delivery_date']:'';
			$data['order_delivery_time'] = !empty($fetchOrder['order_delivery_time']) ? $fetchOrder['order_delivery_time']:'';	
            return $data;
        }
		return false;
	}


	

} 