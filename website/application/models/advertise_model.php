<?php

class Advertise_model extends CI_Model{

    function __construct(){

        parent::__construct();
    }

    function record_count() {

        return $this->db->count_all("records");
    }

    function fetch_records(){

        $database = $this->firebase->init();
		$myads = $database->getReference('MyAds')->getValue();
		
        if (count($myads) > 0){
            return $myads;
        }
        return false;
    }

    function update_status_approve($uid,$advkey) {

		$database = $this->firebase->init();
		$checkstatus = $database->getReference('MyAds/'.$uid)->getChild($advkey.'/status')->getValue();	
		if(trim($checkstatus) != ''){
			$myads = $database->getReference('MyAds/'.$uid)->getChild($advkey.'/status')->set(1);
		}     
    }       
	
	function update_status_reject($uid,$advkey) {

		$database = $this->firebase->init();
		$checkstatus = $database->getReference('MyAds/'.$uid)->getChild($advkey.'/status')->getValue();	
		if(trim($checkstatus) != ''){
			$myads = $database->getReference('MyAds/'.$uid)->getChild($advkey.'/status')->set(2);
		}     
    }   

	function fetch_by_id($uid,$advkey) {

		$database = $this->firebase->init();
		$myads = $database->getReference('MyAds/'.$uid)->getChild($advkey)->getValue();
        return $myads;
    }

    function delete_record($id) {

        $this->db->delete('records', array('id' => $id));
    }

} 