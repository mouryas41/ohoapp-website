<?php

class Admin_model extends CI_Model{

    function login($username, $password){
		
		$database = $this->firebase->init();
		$ohoadmin = $database->getReference('OHOAdmin')->getvalue();
		foreach($ohoadmin as $key => $value){
			if($value['username'] == $username && $value['password'] == $password){
				$data['admin_id'] = $key;
				$data['admin_username'] = $value['username'];
				return $data;
			}
		}
    }

    function save_admin($data){

		$database = $this->firebase->init();
		$ohoadmin = $database->getReference('OHOAdmin')->orderByChild('username')
                    ->equalTo($data['username'])->getValue();
		$exist_userId = array_key_first($ohoadmin); 
		
		if(!empty($exist_userId)){
			return $ohoadmin;
		}else{
			$createTbl = $database->getReference('OHOAdmin')->push($data);
		}
    }

    function update_password_by_id($admin_id,$password){

		$database = $this->firebase->init();
		$ohoadmin = $database->getReference('OHOAdmin')->getChild($admin_id.'/password')->set($password);
    }
}