<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-heading">User Information</div>
            <div class="panel-body">
                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th>Information</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <p>User Id# <?php echo $result->id; ?></p>
                                <p>Email: <?php echo $result->email; ?></p>
                                <p>Name: <?php echo $result->user_name; ?></p>
                   
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer"></div>
        </section>
    </div>
</div>