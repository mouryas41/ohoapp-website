<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-heading">Questions List</div>
            <div class="panel-body">
                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th>Information</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <p>Q Id# <?php echo $result->id; ?></p>
                                <p>Question: <?php echo $result->question; ?></p>
                                <p>Yes: <?php echo $result->yes; ?></p>
                                <p>No: <?php echo $result->no; ?></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer"></div>
        </section>
    </div>
</div>