<div class="row">
    <div class="col-lg-6">
        <section class="panel">
            <div class="panel-body">
                <div class="form">
				<h3>Select an audio file from your computer and upload</h3>
				<p style="color:red;"><?php echo isset($msg['error']) ? $msg['error'] :''; ?></p>
				<p style="color:green;"><?php echo isset($msg['msg']) ? $msg['msg'] :''; ?></p>
				<br>
				
				<?php if(!empty($result)){ ?>
				<form class="form-validate form-horizontal" method="post" action="<?php echo base_url(); ?>master/update_commit" enctype="multipart/form-data">
				
					<div class="form-group">
						<label for="question" class="control-label col-lg-2">Audio File</label>
						<div class="col-lg-10">
							<input type="hidden" id="file_id" name="file_id" value="<?php echo $result->id; ?>"/>
							<input type="file" id="audio_file" name="audio_file"/>
						</div>
					</div>
					<div class="form-group">
						<label for="yes" class="control-label col-lg-2">Weight</label>
						<div class="col-lg-10">
							<input type="number" id="weight" name="weight" value="<?php echo $result->weight; ?>">
						</div>
					</div>
				<?php 
					}else{
				?>
				<form class="form-validate form-horizontal" method="post" action="<?php echo base_url(); ?>master/store" enctype="multipart/form-data">
					<div class="form-group">
						<label for="question" class="control-label col-lg-2">Audio File</label>
						<div class="col-lg-10">
							<input type="file" id="audio_file" name="audio_file"/>
						</div>
					</div>
					<div class="form-group">
						<label for="yes" class="control-label col-lg-2">Weight</label>
						<div class="col-lg-10">
							<input type="number" id="weight" name="weight"/>
						</div>
					</div>
				<?php 
					}
				?>
					<div class="form-group">
						<div class="col-lg-offset-2 col-lg-10">
							<button class="btn btn-primary" type="submit">Save</button>
						</div>
					</div>
				</form>
                </div>
            </div>
        </section>
    </div>
</div>