<?php require_once("modal/add_employee.php"); ?>
<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
	$('#myTable').DataTable({
		"paging": true,	  
		"searching": false,
		"ordering": true,
		"info": false,
		"lengthChange": false,
		"autoWidth": false,
	});
});
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="panel-body">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-8">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-responsive table-striped table-advance table-hover" id="myTable">
                    <thead>
                        <tr>
                            <th><i class=""></i> Sr No.</th>
                            <th><i class=""></i> Recorded file</th>
                            <th><i class=""></i> Result(%)</th>
                        </tr>
                    </thead>
					</audio>
                    <tbody>
                    <?php 
					if(!empty($results)){
						$i = 1;
						foreach($results as $list){ ?>
                        <tr>
							<td><?php echo $i++; ?></td>
                            <td><?php echo $list->audio_file; ?></td>
                            <td><?php echo $list->match_pct; ?></td>
                        </tr>
                    <?php 
						}
					} 
					?>
                </table>
            </div>
        </section>
        <div class="text-center"><p><?php //echo $links; ?></p></div>
    </div>
</div>