<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="form">
                    <form class="form-validate form-horizontal" id="feedback_form" method="post" action="<?php echo base_url(); ?>question/add_question.html">
                        <div class="form-group">
                            <label for="question" class="control-label col-lg-2">Question</label>
                            <div class="col-lg-10">
                                <input class="form-control" name="question" type="text" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="yes" class="control-label col-lg-2">Yes</label>
                            <div class="col-lg-10">
                                <input class="form-control" type="text" name="yes" value=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="no" class="control-label col-lg-2">No</label>
                            <div class="col-lg-10">
                                <input class="form-control" type="text" name="no" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-primary" type="submit">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
