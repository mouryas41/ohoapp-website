<?php require_once("modal/add_employee.php"); ?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel-body">
            <a href="<?php echo base_url(); ?>question/add/" class="btn btn-primary pull-left">Add New</a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-responsive table-striped table-advance table-hover">
                    <thead>
                        <tr>
                            <th><i></i> Q No.</th>
                            <th><i class="icon_datareport"></i> Question</th>
                            <th><i class=""></i> Yes</th>
                            <th><i class=""></i> No</th>
                            <th><i class="icon_clipboard"></i> Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($results as $list){ ?>
                        <tr>
                            <td>
                                <?php echo $list->id; ?>
                            </td>
							<td>
                                <a href="<?php echo base_url(); ?>question/view/<?php echo $list->id; ?>"><?php echo $list->question; ?></a>
                            </td>
                            <td><?php echo $list->yes; ?></td>
                            <td><?php echo $list->no; ?></td>
                            <td>
                                <a href="<?php echo base_url(); ?>question/update/<?php echo $list->id; ?>">Edit</a>
								|
								<a href="<?php echo base_url(); ?>question/delete_question/<?php echo $list->id; ?>">Delete</a>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </section>
        <div class="text-center"><p><?php echo $links; ?></p></div>
    </div>
</div>