<?php require_once("modal/add_employee.php"); ?>
<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
	$('#myTable').DataTable({
		"paging": true,	  
		"searching": false,
		"ordering": true,
		"info": false,
		"lengthChange": false,
		"autoWidth": true,
	});
});
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="panel-body">
			<a href="<?php echo base_url(); ?>master/add/" class="btn btn-primary pull-left">Add New</a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-responsive table-striped table-advance table-hover" id="myTable">
                    <thead>
                        <tr>
                            <th><i class=""></i> Sr No.</th>
                            <th><i class="icon_profile"></i> Audio File</th>
                            <th></th>
                            <th><i class="icon_datareport"></i> Weight</th>
                            <th><i class="icon_clipboard"></i> Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
					$i = 1;
					foreach($results as $list){ ?>
                        <tr>
							<td><?php echo $i; ?></td>
                            <td>
                               <?php echo $list->audio_file; ?>
							</td>
							<td>  
								<audio controls src="../../voiceapp/audio_files/main_files/<?php echo $list->audio_file; ?>">
                            </td>
                            <td> <?php echo $list->weight; ?></td>
                            <td>
								<a href="<?php echo base_url(); ?>master/update/<?php echo $list->id; ?>">Edit</a>
								|
								<a href="<?php echo base_url(); ?>master/delete_file/<?php echo $list->id; ?>" onclick="return confirm('Are you sure you want to delete this file?');">Delete</a>
                            </td>
                        </tr>
                    <?php $i++;
					} ?>
                </table>
            </div>
        </section>
        <div class="text-center"><p><?php //echo $links; ?></p></div>
    </div>
</div>