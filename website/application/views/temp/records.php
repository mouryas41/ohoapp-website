<?php require_once("modal/add_employee.php"); ?>
<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
	$('#myTable').DataTable({
		"paging": true,	  
		"searching": true,
		"ordering": true,
		"info": false,
		"lengthChange": false,
		"autoWidth": true,
	});
});
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="panel-body">
            <!--<button class="btn btn-primary pull-left" id="add">Add New</button>-->
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-responsive table-striped table-advance table-hover" id="myTable">
                    <thead>
                        <tr>
                            <th><i class=""></i> Sr No.</th>
                            <th><i class="icon_profile"></i> User ID</th>
                            <th><i class=""></i> Profile Name</th>
                            <th><i class=""></i> Name</th>
                            <th><i class=""></i> Age</th>
                            <th><i class=""></i> Weight</th>
                            <th><i class=""></i> Height</th>
                            <th><i class=""></i> Temperature</th>
                            <th><i class="icon_clipboard"></i> Action</th>
                        </tr>
                    </thead>
					</audio>
                    <tbody>
                    <?php 
					if(!empty($results)){
						$i = 1;
						foreach($results as $list){ ?>
                        <tr>
							<td><?php echo $i++; ?></td>
                            <td>
                                <a href="<?php echo base_url(); ?>records/view/<?php echo $list->user_id; ?>"><?php echo $list->user_id; ?></a>
                            </td>
                            <td><?php echo $list->profile; ?></td>
                            <td><?php echo $list->name; ?></td>
                            <td><?php echo $list->age; ?></td>
                            <td><?php echo $list->weight; ?></td>
                            <td><?php echo $list->height; ?></td>
                            <td><?php echo $list->temperature; ?></td>
                            <td>
								<a href="<?php echo base_url(); ?>records/voice_detail/<?php echo $list->id; ?>">View Record</a>
								
								|
								<a href="<?php echo base_url(); ?>records/delete_record/<?php echo $list->id; ?>" onclick="return confirm('Are you sure you want to delete?');">Delete</a>
                            </td>
                        </tr>
                    <?php }
					}
					?>
                </table>
            </div>
        </section>
        <div class="text-center"><p><?php //echo $links; ?></p></div>
    </div>
</div>