<div class="row">
    <div class="col-lg-12">
    	<h3><?php echo $heading; ?></h3>
    	<hr>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <section class="panel">			
			<div class="panel-heading">Import Excel,CSV and Images</div>
            <div class="panel-body">
				<p><?php if(!empty($message)){ echo $message; } ?></p>						
				<form enctype="multipart/form-data" method="post" role="form">

					<div class="form-group mb-2">
						<label class="col-form-label">Select Vendor</label>
						<select name="vendor" id="vendor" class="form-control">
							<option value="">Select Vendor</option>
							<?php 
							if(!empty($vendors)){	
								foreach($vendors as $vendor_id => $vendorInfo){									
									$vendor_name = !empty($vendorInfo['user_name']) ? $vendorInfo['user_name']:'';
							?>
								<option value="<?php echo $vendor_id; ?>"><?php echo $vendor_id; ?> (<?php echo $vendor_name; ?>)</option>
							<?php
								}
							}
							?>
						</select>						
					</div>

					<div class="form-group mx-sm-3 mb-2">
						<label class="col-form-label">File Upload</label>
						<input type="file" name="file" id="file" class="form-control">						
					</div>
					<button type="submit" class="btn btn-primary" name="submit" value="submit" onclick="loaderPlay();">Upload</button>
				</form>
            </div>
        </section> 
    </div>
</div>