<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
	$('#startdate').datepicker({ dateFormat: 'dd-mm-yy' });
	$('#enddate').datepicker({ dateFormat: 'dd-mm-yy' });
	
	$('#myTable').DataTable({
		dom: 'Bfrtip',
        buttons:[
			{ 
				extend: 'excel',
				text: 'Export'
			}
        ]
	});	
});

function resetForm(){
    document.getElementById("myForm").reset();
    document.getElementById("startdate").value = '';
    document.getElementById("enddate").value='';	
	location.reload(true); 
}
</script>

<div class="row">
    <div class="col-lg-12">
    	<h3><?php echo $heading; ?></h3>
    	<hr>

    	<section class="panel">			
    		<div class="panel-body">    			
				<div class="form-group">    					
					<a href="<?php echo base_url(); ?>products/import.html" onclick="loaderPlay();">
						<button type="button" class="btn btn-primary btn-rounded float-right">Import CSV</button>
					</a>
				</div>    				    			
    		</div>
    	</section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
			<div class="panel-heading">Orders</div>
            <div class="panel-body">						
                <table class="display table" id="myTable">
                    <thead>
                        <tr>
                            <th><i class=""></i> Sr No.</th>                            
							<th><i class=""></i>Product</th> 
							<th><i class=""></i>Name</th>                            
                            <th><i class="icon_datareport"></i>Price</th>                            
                            <th><i class="icon_datareport"></i>Stock</th>                                                                                   
							<th><i class="icon_profile"></i>category</th>
                            <th><i class="icon_clipboard"></i> Vendor</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 					
					if(!empty($results)){
						$i = 1;	
						foreach($results as $rows){ 							
					?>
                       <tr>
							<td><?php echo $i; ?></td> 							                                                                                    							
							<?php 
							if(!empty($rows['image'])){
								$image = $rows['image'];
							}else{
								$image = 'https://firebasestorage.googleapis.com/v0/b/ohoo-114f5.appspot.com/o/uploads%2Fdefault_category.jpg?alt=media&token=8335f4d2-3886-4a45-bfa2-dd0ca7271e1b';
							}
							?>	
							<td><img src="<?php echo $image; ?>" width='50px' height="50px"></td>						
                            <td><?php echo $rows['title']; ?></td> 
							<td><?php echo $rows['price']; ?></td>                            
                            <td><?php echo $rows['quantity']; ?></td>                                                                                     						
							<td><?php echo $rows['category']; ?></td>
							<td><?php echo $rows['vendor_name']['user_name']; ?></td>							
                        </tr>
                    <?php 
						$i++;							
						}
					}				
					?>
					</tbody>
                </table>
            </div>
        </section> 
    </div>
</div>