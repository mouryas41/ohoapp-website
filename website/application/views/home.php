<div class="row">
	
<?php
//machines sales
$mdatapoints = array();
$mtotal = 0;
foreach($mdata as $mcdata){
	$temp = [];	
    $temp['y'] = $mcdata['price'] * $mcdata['quentity'];    
    $temp['label'] = $mcdata['order_date'];
	$mdatapoints[] = $temp;
	$mtotal+= $mcdata['price'] * $mcdata['quentity'];
}


//vegetables sales
$vdatapoints = array();
$vtotal = 0;
foreach($vdata as $vegdata){
	$temp = [];	
    $temp['y'] = $vegdata['price'] * $vegdata['quentity'];    
	$temp['label'] = $vegdata['order_date'];
	$vdatapoints[] = $temp;
	$vtotal+= $vegdata['price'] * $vegdata['quentity'];
}
?>
<script src="<?php echo base_url(); ?>js/canvasjs.min.js"></script>
<script>
window.onload = function () {
 
var mchart = new CanvasJS.Chart("machineSaleschart", {
	title: {
		text: "Machines's Sales"
	},
	axisY: {
		title: "Profit"
	},
	data: [{
		type: "line",
		dataPoints: <?php echo json_encode($mdatapoints, JSON_NUMERIC_CHECK); ?>
	}]
});
mchart.render();

var vchart = new CanvasJS.Chart("vegSaleschart", {
	title: {
		text: "Vegetables's Sales"
	},
	axisY: {
		title: "Profit"
	},
	data: [{
		type: "line",
		dataPoints: <?php echo json_encode($vdatapoints, JSON_NUMERIC_CHECK); ?>
	}]
});
vchart.render();
 
}
</script>
	<div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
              <b>Total Machinary Sales Amount :- <?php echo $mtotal; ?></b>
            </div>
        </section>
    </div>	
	<div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
              <b>Total Vegetables Sales Amount :- <?php echo $vtotal; ?></b>
            </div>
        </section>
    </div>
	<div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
               <div id="machineSaleschart" style="height: 250px; width: 100%;"></div>	
            </div>
        </section>
    </div>	
	<div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
               <div id="vegSaleschart" style="height: 250px; width: 100%;"></div>	
            </div>
        </section>
    </div>	
	
	<!--<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <a href="<?php //echo base_url(); ?>site/employee.html">
            <div class="info-box blue-bg">
                <div class="count">Questions</div>
                <div class="title">Manage questions</div>
            </div>
        </a>
    </div>-->
	
   <!-- <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <a href="<?php //echo base_url(); ?>site/settings.html">
            <div class="info-box blue-bg">
                <div class="count">Settings</div>
                <div class="title">Manage settings</div>
            </div>
        </a>
    </div><!--/.col-->
</div>

