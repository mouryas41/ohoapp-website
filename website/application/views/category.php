<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
	$('#myTable').DataTable({
		"paging": true,	  
		"searching": true,
		"ordering": true,
		"info": false,
		"lengthChange": false,
		"autoWidth": true,
		
	});
});
</script>

<h3><?php echo $heading; ?></h3>
<hr>

<div class="row">

    <div class="col-lg-4">                      
       <!-- Add Edit category -->                     
       <section class="panel">
            <div class="panel-heading">Category Form</div>
            <div class="panel-body">   

                <div id="success_message" class="alert alert-success" style="display:none;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                </div>
                <div id="error_message" class="alert alert-danger" style="display:none;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                </div>

                <form id="myform" method="POST" action="" enctype="multipart/form-data">

                    <div class="box-body">
                        <div class="form-group">
                            <label for="category" class="col-md-4 control-label">Category</label>
                            <div class="col-md-12">  
                            <?php if(!empty($exist_category)){ ?>                                                             
                                <input id="category" type="text" class="form-control" name="category" value="<?php echo $exist_category['category']; ?>">
                            <?php }else{ ?>
                                <input id="category" type="text" class="form-control" name="category" value="">
                            <?php } ?>                                    
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4"> 
                            <?php if(!empty($exist_category)){ ?>
                                <button type="button" class="btn btn-primary" onclick="formUSubmit();">Update</button>
                            <?php }else{ ?>
                                <button type="button" class="btn btn-primary" onclick="formSubmit();">Save</button>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </section>           
    </div>

    <div class="col-lg-8">
        <section class="panel">  
            <div class="panel-heading">Categories</div>      
            <div class="panel-body">
                <div id="delete_success_message" class="alert alert-success" style="display:none;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                </div>
                <table class="table table-responsive table-striped table-advance table-hover" id="myTable">
                    <thead>
                        <tr>
                            <th><i class=""></i>Sr No.</th>                            
                            <th><i class=""></i>Category</th>
                            <th><i class=""></i>Action</th>														
                        </tr>
                    </thead>
                    <tbody id="category_list">
                    <?php 
                    $i = 1; 
                    if(!empty($results)){
                        foreach($results as $key => $value){ 
                        ?>
                            <tr>
                                <td><?php echo $i; ?></td>							
                                <td><?php echo $key; ?></td>                            
                                <td>                                
                                    <a class="btn btn-danger btn-xs" onclick="deleteCategory('<?php echo $key; ?>');">Delete</a>
                                </td>
                            </tr>
                        <?php $i++;
                        } 
                    }
					?>
                </table>
            </div>
        </section>        
    </div>
</div>


<script src="http://localhost/website/js/jquery.js"></script>
<script src="http://localhost/website/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

    $("form").attr('autocomplete', 'off');

    setInterval(function(){ 
        $('#error_message, #success_message, #delete_success_message').hide();
    }, 5000);
})

var loadFile = function(event) {
    var output = document.getElementById('uploaded_image');
    output.src = URL.createObjectURL(event.target.files[0]);
    $("#uploaded_image").show();
    output.onload = function() {
    URL.revokeObjectURL(output.src) // free memory
    }
};


/* Add subcategory info */
function formSubmit(){ 

    $('.loading').show();   
    var form_data = new FormData(document.getElementById("myform"));        
    $.ajax({
        url: "<?php echo base_url(); ?>category/add",
        type: "post",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,           
        success: function(response) {  
            $('.loading').hide();  
            console.log(response);
            if(response.status == 'error'){
                 $("#error_message").html(response.message).show();
            }else{                
                $("#success_message").html(response.message).show(); 
                $("#category_list").empty();
                $("#category_list").html(response.table);   
                $("#myform")[0].reset()                                           
            }
        }
    });
}

/* Update subcategory info */
function deleteCategory(category){ 

    var confirmalert = confirm('Are You sure want to delete this ?');
    if (confirmalert == true){
        $('.loading').show();     
        formData = {
            category: category
        };
        $.ajax({
            url: "<?php echo base_url(); ?>category/delete",
            type: "post",
            data: formData,                  
            success: function(response) { 
                //console.log(response);           
                $('.loading').hide();  
                if(response.status == 'error'){
                     $("#error_message").html(response.message).show();
                }else{  
                    //console.log(response);
                    $("#delete_success_message").html(response.message).show();
                    $("#category_list").empty();
                    $("#category_list").html(response.table);                                                    
                }
            }
        });
    }
}
</script>