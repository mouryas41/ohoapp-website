<h3><?php echo $heading; ?></h3>
<hr>
<?php
function get_file_path_name($a)
{
    $explode = explode("%2F",$a);
    $part2 = !empty($explode) ? $explode['1']:'';
    $arr = !empty($part2) ? explode("?", $part2):'';
    $filename = !empty($arr) ? $arr['0']:''; 
    return $filename;
}
?>

<div class="row">
    
    <div class="col-lg-4"> 
        <!-- Add Edit subcategory -->                     
        <section class="panel">
            <div class="panel-heading">Sub-Category Form</div>
            <div class="panel-body">   

                <div id="success_message" class="alert alert-success" style="display:none;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                </div>
                <div id="error_message" class="alert alert-danger" style="display:none;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                </div>

                <form id="myform" method="POST" action="" enctype="multipart/form-data">

                    <div class="box-body">
                        <div class="form-group">
                            <label for="subcategory" class="col-md-4 control-label">Sub-Category</label>
                            <div class="col-md-12">  
                            <?php if(!empty($exist_category)){ ?>                             
                                <input id="subcategory_id" type="hidden" class="form-control" name="subcategory_id" value="<?php echo $exist_category['id']; ?>">
                                <input id="subcategory" type="text" class="form-control" name="subcategory" value="<?php echo $exist_category['title']; ?>">
                            <?php }else{ ?>
                                <input id="subcategory" type="text" class="form-control" name="subcategory" value="">
                            <?php } ?>                                    
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="category" class="col-md-4 control-label">Category</label>
                            <div class="col-md-12">                              
                                <select id="category" class="form-control" name="category">                                        
                                    <option value="0">Select Category</option>
                                    <?php
                                    if(count($categories) > 0){
                                        foreach($categories as $category => $categoryarray){
                                            if(!empty($exist_category['category']) && $category == $exist_category['category']){
                                                echo '<option value="'.$category.'" selected>'.$category.'</option>';
                                            }else{
                                                echo '<option value="'.$category.'">'.$category.'</option>';
                                            }
                                        }
                                    }
                                    ?>
                                </select>                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="picture" class="col-md-4 control-label">Image</label>                        
                            <div class="col-md-12">
                                <?php if(!empty($exist_category)){ ?>                                   
                                    <input type="file" onchange="loadFile(event)" name="picture" id="picture" accept="image/x-png,image/gif,image/jpeg" class="form-control">
                                    <input type="hidden" name="old_picture" id="old_picture" class="form-control" value="<?php echo $exist_category['image']; ?>">                                    
                                <?php }else{ ?>
                                    <input type="file" onchange="loadFile(event)" name="picture" id="picture" accept="image/x-png,image/gif,image/jpeg" class="form-control">                                    
                                <?php } ?>

                                <?php if(!empty($exist_category['image'])){ ?>
                                    <img id="uploaded_image" width="50px" height="50px" src="<?php echo $exist_category['image']; ?>">
                                <?php }else{ ?>
                                    <img id="uploaded_image" width="50px" height="50px" style="display:none;">
                                <?php } ?>  
                            </div>                                   
                        </div>
                                                    
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4"> 
                            <?php if(!empty($exist_category)){ ?>
                                <button type="button" class="btn btn-primary" onclick="formUSubmit();">Update</button>
                            <?php }else{ ?>
                                <button type="button" class="btn btn-primary" onclick="formSubmit();">Save</button>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </section>        
    </div>

    <!-- Listing of subcategories -->
    <div class="col-lg-8">              
        <section class="panel">
            <div class="panel-heading">Sub-Categories</div>
            <div class="panel-body">
                <div id="delete_success_message" class="alert alert-success" style="display:none;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                </div>
                <table class="table table-responsive table-striped table-advance table-hover" id="myTable">
                    <thead>
                        <tr>
                            <th><i class=""></i>Sr No.</th>  
                            <th><i class=""></i>Image</th>                          
                            <th><i class=""></i>Sub Category</th>
                            <th><i class=""></i>Category</th>
                            <th><i class=""></i>Action</th>															
                        </tr>
                    </thead>
                    <tbody id="category_list">
                    <?php 
                    $i = 1; 
                    if(!empty($results)){
                        foreach($results as $key => $value){ 
                    ?>
                            <tr>
                                <td><?php echo $i; ?></td>	
                                  <?php 
                                    if(!empty($value['image'])){
                                        $image = $value['image'];
                                    }else{
                                        $image = 'https://firebasestorage.googleapis.com/v0/b/ohoo-114f5.appspot.com/o/uploads%2Fdefault_category.jpg?alt=media&token=8335f4d2-3886-4a45-bfa2-dd0ca7271e1b';
                                    }
                                  ?>
                                <td><img src="<?php echo $image; ?>" width='50px' height="50px"></td>   
                                <td><?php echo $value['title']; ?></td>   						
                                <td><?php echo $value['category']; ?></td>                                                        
                                <td>
                                    <a href="<?php echo base_url('category/subcategory/edit') .'/'. $value['category'] .'/'. $value['id']; ?>" class="btn btn-primary btn-xs">Edit</a>
                                    |
                                  <?php
                                    $filename = !empty($value['image']) ? get_file_path_name($value['image']):'';
                                    $filePathName = !empty($filename) ? 'uploads/'.$filename:'';
                                  ?>
                                    <a onclick="deleteSubcategory('<?php echo $value['category']; ?>','<?php echo $value['id']; ?>','<?php echo $filePathName; ?>');" class="btn btn-danger btn-xs">Delete</a>
                                </td>
                            </tr>
                    <?php 
                            $i++;
                        } 
                    }
					?>                    
                </table>
            </div>
        </section>  
        
    </div>
    <!-- End Listing of subcategories -->
</div>

<script src="http://localhost/website/js/jquery.js"></script>
<script src="http://localhost/website/js/jquery-1.8.3.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.8.1/firebase.js"></script>
<script src="<?php echo base_url('script/firebase.js'); ?>"></script>
<script language="JavaScript" type="text/javascript">
$(document).ready(function(){

    $("form").attr('autocomplete', 'off');

	$('#myTable').DataTable({
        "paging": true,	  
        "searching": true,
        "ordering": true,
        "info": false,
        "lengthChange": false,
        "autoWidth": true,		
    });

    setInterval(function(){ 
        $('#error_message, #success_message, #delete_success_message').hide();
    }, 5000);
});

//show image before upload
var loadFile = function(event) {
    var output = document.getElementById('uploaded_image');
    output.src = URL.createObjectURL(event.target.files[0]);
    $("#uploaded_image").show();
    output.onload = function() {
    URL.revokeObjectURL(output.src) // free memory
    }
};


/* Add subcategory image */
function formSubmit(){ 

    $('.loading').show();   
    var form_data = new FormData(document.getElementById("myform")); 

    var error = [];
    var subcategory =  $('#subcategory').val();
    if(subcategory == ''){       
        error.push('<li>The Sub-Category field is required.</li>');                         
    }    

    var files = $('#picture')[0].files[0];                 
    if($.type(files) == 'null' || $.type(files) == 'undefined' || $.trim(files) == ''){       
        error.push('<li>The Image field is required.</li>');                                
    }
   
    if (error.length === 0) 
    {
        /***************** Start Upload image **********************/
        // File or Blob named mountains.jpg
        var file = files;

        // Create the file metadata
        var metadata = {
            contentType: 'image/jpeg'
        };

        // Upload file and metadata to the object 'images/mountains.jpg'
        var uploadTask = storageRef.child('uploads/' + file.name).put(file, metadata);
        
        var array = [];
        // Listen for state changes, errors, and completion of the upload.
        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
        function(snapshot) {

            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            /*var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED: // or 'paused'
                    console.log('Upload is paused');
                    break;
                case firebase.storage.TaskState.RUNNING: // or 'running'
                    console.log('Upload is running');
                    break;
            }*/

        }, function(error) {   

            // A full list of error codes is available at
            // https://firebase.google.com/docs/storage/web/handle-errors
            /* switch (error.code) {
                case 'storage/unauthorized':
                // User doesn't have permission to access the object
                break;

                case 'storage/canceled':
                // User canceled the upload
                break;

                case 'storage/unknown':
                // Unknown error occurred, inspect error.serverResponse
                break;
            } */   

        }, function() {  

            // Upload completed successfully, now we can get the download URL
            uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL){
                //console.log('File available at', downloadURL);
                
                //Download Url append in form_data and store in table
                form_data.append('image_url',downloadURL);                    
                addUpload(form_data);                                                  
            });                                             
        });  
    }
    else
    {
        $('.loading').hide(); 
        $('#error_message').html(error).show();        
    }    
}


/* Add subcategory info */
function addUpload(form_data){

    $.ajax({
        url: "<?php echo base_url(); ?>category/subcategory/add",
        type: "post",
        data: form_data,
        contentType: false,            
        cache: false,
        processData: false,           
        success: function(response) {              
            $('.loading').hide();  
            if(response.status == 'error'){
                $("#error_message").html(response.message).show();
            }else{ 
                $("#myform")[0].reset();  
                $("#uploaded_image").hide();                 
                $("#success_message").html(response.message).show();
                $("#category_list").empty();
                $("#category_list").html(response.table);                                
            }
        }
    });
}


/* update subcategory image */
function formUSubmit(){ 

    $('.loading').show();   
    var form_data = new FormData(document.getElementById("myform")); 

    var error = [];
    var subcategory =  $('#subcategory').val();
    if(subcategory == ''){       
        error.push('<li>The Sub-Category field is required.</li>');                         
    }  

    if (error.length === 0) 
    {
        var files = $('#picture')[0].files[0];                     
        if($.type(files) != 'null' && $.type(files) != 'undefined' && $.trim(files) != '')          
        {    

            /***************** Start Upload image **********************/
            // File or Blob named mountains.jpg
            var file = files;

            // Create the file metadata
            var metadata = {
                contentType: 'image/jpeg'
            };

            // Upload file and metadata to the object 'images/mountains.jpg'
            var uploadTask = storageRef.child('uploads/' + file.name).put(file, metadata);
            
            var array = [];
            // Listen for state changes, errors, and completion of the upload.
            uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
            function(snapshot) {

                // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                /*var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                console.log('Upload is ' + progress + '% done');
                switch (snapshot.state) {
                    case firebase.storage.TaskState.PAUSED: // or 'paused'
                        console.log('Upload is paused');
                        break;
                    case firebase.storage.TaskState.RUNNING: // or 'running'
                        console.log('Upload is running');
                        break;
                }*/

            }, function(error) {   

                // A full list of error codes is available at
                // https://firebase.google.com/docs/storage/web/handle-errors
                /* switch (error.code) {
                    case 'storage/unauthorized':
                    // User doesn't have permission to access the object
                    break;

                    case 'storage/canceled':
                    // User canceled the upload
                    break;

                    case 'storage/unknown':
                    // Unknown error occurred, inspect error.serverResponse
                    break;
                } */   

            }, function() {  

                // Upload completed successfully, now we can get the download URL
                uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL){
                    //console.log('File available at', downloadURL);
                    
                    //Download Url append in form_data and store in table
                    form_data.append('image_url',downloadURL);                    
                    updateUpload(form_data);                                                  
                });                                             
            });  
        }
        else
        {            
            updateUpload(form_data);         
        } 
    }
    else
    {
        $('.loading').hide(); 
        $('#error_message').html(error).show();        
    }        
}


/* Update subcategory info */
function updateUpload(form_data){  
        
    $.ajax({
        url: "<?php echo base_url(); ?>category/subcategory/update",
        type: "post",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,       
        success: function(response) {  
            //console.log(response);          
            $('.loading').hide();  
            if(response.status == 'error'){
                 $("#error_message").html(response.message).show();
            }else{  
                $("#success_message").html(response.message).show();
                loaderPlay();
                $(location).attr('href', '<?php echo base_url(); ?>category/subcategory');                          
            }
        }
    });
}


/* Update subcategory info */
function deleteSubcategory(category, sub_catid, filePathName){ 

    var confirmalert = confirm('Are You sure want to delete this ?');
    if (confirmalert == true) {
        $('.loading').show();     
        formData = {
            category: category,		
            sub_catid: sub_catid,
        };
        $.ajax({
            url: "<?php echo base_url(); ?>category/subcategory/delete",
            type: "post",
            data: formData,                  
            success: function(response) { 
                //console.log(response);           
                $('.loading').hide();  
                if(response.status == 'error'){
                     $("#error_message").html(response.message).show();
                }else{  
                    $("#delete_success_message").html(response.message).show();
                    $("#category_list").empty();
                    $("#category_list").html(response.table); 
                    
                    // Delete the file from storage firebase
                    if(filePathName != ''){                        
                        var desertRef = storageRef.child(filePathName);
                        desertRef.delete().then(function() {
                            // File deleted successfully
                        }).catch(function(error) {
                            // Uh-oh, an error occurred!
                        });  
                    }
                    location.reload();                                                       
                }
            }
        });
    }
}
</script>