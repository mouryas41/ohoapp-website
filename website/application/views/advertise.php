<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
	
	// Setup - add a select option to each header cell
    $('#myTable thead tr').clone(true).appendTo('#myTable thead');
    $('#myTable thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
		if(title == 'Machine Type'){
			
			/* $(this).html( '<input type="text" placeholder="Search '+title+'"/><br>' + title);	 
			$('input',this).on('keyup change', function () {				
				if (table.column(i).search() !== this.value){
					table
						.column(i)
						.search( this.value )
						.draw();
				}
			}); */
			
			$(this).html('<select id="#msds-select" class="form-control"><option value="">Select Type</option><option value="new">New</option><option value="old">Old</option><option value="rent">Rent</option></select> <br>' + title);
			$(this).children().on('change', function(){
				if (table.column(i).search() !== this.value){
					var val = this.value;
					table
						.column(i)
						.search( val ? '^'+val+'$' : '', true, false )
						.draw();
				}
			});
			
		}
    });	
			
	var table = $('#myTable').DataTable({
		"paging": true,	  
		"searching": true,
		"ordering": true,
		"info": false,
		"lengthChange": false,
		"autoWidth": true,
		orderCellsTop: true,
        fixedHeader: true
	});
	$('#myTable thead tr:first').hide();

});
</script>

<div class="row">
    <div class="col-lg-12">
		<h3><?php echo $heading; ?></h3><hr>
        <section class="panel">
			<div class="panel-heading">Advertise List</div>
            <div class="panel-body">				
                <table class="table table-responsive table-striped table-advance table-hover" id="myTable">
                    <thead>
                        <tr>
                            <th><i class=""></i> Sr No.</th>                            
                            <th><i class=""></i>Image</th>
                            <th><i class=""></i>Title</th>
							<th><i class=""></i>Machine</th>
                            <th><i class=""></i>Machine Type</th>
							<th><i class=""></i>Description</th>
                            <th><i class=""></i>Price</th>
							<th><i class=""></i>Date</th>
							<th><i class="icon_profile"></i>Provider</th>
                            <th><i class="icon_clipboard"></i> Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
					if(!empty($results)){
						$i = 1;
						foreach($results as $key => $list){ 
							foreach($list as $ckey => $cvalue){ 							
							$userId = $cvalue['userDetails'][0]['user_id'];
						?>
                        <tr>
							<td><?php echo $i++; ?></td>							
							<td><img src="<?php echo $cvalue['images'][0]; ?>" width="50px" height="50px"></td>
                            <td><?php echo $cvalue['title']; ?></td>
                            <td><?php echo $cvalue['machine']; ?></td>
                            <td><?php echo $cvalue['type_of_machine']; ?></td>
                            <td width="20px"><?php echo $cvalue['description']; ?></td>
                            <td><?php echo $cvalue['price']; ?></td>
                            <td style='white-space: nowrap;'><?php echo $cvalue['date']; ?></td>
							<td>
								<a href="<?php echo base_url(); ?>advertise/view/<?php echo $userId.'/'.$ckey; ?>">
								<?php echo $userId; ?>
								</a>
							</td>
                            <td style='white-space: nowrap;'>
								<?php if(isset($cvalue['status']) && $cvalue['status'] == 1){ ?>								
									<button class="btn btn-success btn-sm">Approved</button>	
								<?php }elseif(!empty($cvalue['status']) && $cvalue['status'] == 2){ ?>								
									<button class="btn btn-danger btn-sm">Rejected</button>										
								<?php }else{ ?>
									<a href="<?php echo base_url(); ?>advertise/approve/<?php echo $userId.'/'.$ckey; ?>">
										<button type="button" class="btn btn-success btn-sm">Approve</button>
									</a> 
								<?php } ?>
								
								<?php if(isset($cvalue['status']) && $cvalue['status'] == 0){ ?>	
									<a href="<?php echo base_url(); ?>advertise/reject/<?php echo $userId.'/'.$ckey; ?>" onclick="return confirm('Are you sure you want to Reject ?');">
										<button type="button" class="btn btn-danger btn-sm">Reject</button>
									</a>
								<?php } ?>
                            </td> 
                        </tr>
						<?php }
						}
					}
					?>
					</tbody>
                </table>
            </div>
        </section>
    </div>
</div>