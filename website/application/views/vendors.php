<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
	$('#myTable').DataTable({
		"paging": true,	  
		"searching": true,
		"ordering": true,
		"info": false,
		"lengthChange": false,
		"autoWidth": true,
	});
});
</script>

<div class="row">
    <div class="col-lg-12">	
        <h3><?php echo $heading; ?></h3><hr>	
        <section class="panel">
            <div class="panel-heading">Vendors List</div>
            <div class="panel-body">			
                <table class="table table-responsive table-striped table-advance table-hover" id="myTable">
                    <thead>
                        <tr>
                            <th><i class=""></i> Sr No.</th>
                            <th><i class="icon_profile"></i> Profile</th>
                            <th><i class=""></i> Vendor Name</th>
                            <th><i class=""></i> Mobile</th>
                            <th><i class=""></i> Member Since</th>
							<!-- <th><i class=""></i> Address</th> -->
                            <th><i class="icon_clipboard"></i> Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
					if(!empty($results)){
						$i = 1;
						//Comparison function 
						function date_compare($element1, $element2){							
							$datetime1 = strtotime(trim($element1['member_since_date'])); 
							$datetime2 = strtotime(trim($element2['member_since_date'])); 
							if($element1['member_since_date']==$element2['member_since_date']) return 0;
							return $element1['member_since_date'] > $element2['member_since_date']?1:-1;
						}  
						//Sort the array in decending order  
						usort($results, 'date_compare');

						foreach($results as $key => $element){ 							
						?>
                        <tr>
							<td><?php echo $i++; ?></td>
                            <td>
								<img src="<?php echo !empty($user['profile_image']) ? $user['profile_image'] : base_url('images/default.jpg'); ?>" width="50px" height="50px">
                            </td>
                            <td><?php echo $element['user_name']; ?></td>
                            <td><?php echo $element['mobile_no']; ?></td>
                            <td><?php echo $element['member_since_date']; ?></td>
                            <!-- <td><?php //echo $element['address']; ?></td> -->
                            <td>
                                <a href="<?php echo base_url(); ?>vendors/view/<?php echo $element['user_id']; ?>" class="btn btn-primary btn-sm" onclick="loaderPlay();">View</a>
								<!-- <a href="<?php //echo base_url(); ?>vendors/delete_record/<?php //echo $list['user_id']; ?>" onclick="return confirm('Are you sure you want to delete?');">Delete</a> -->
                            </td>
                        </tr>
                    <?php }
					}
					?>
                </table>
            </div>
        </section>
    </div>
</div>