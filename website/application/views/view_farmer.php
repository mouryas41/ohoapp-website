<div class="row">
    <div class="col-lg-12">

	<div class="row">
		<div class="col-md-8">
			<h3><?php echo $heading; ?></h3>
		</div>
		<div class="col-md-4">
			<h3><a href="javascript:window.history.go(-1);" onclick="loaderPlay();" class="btn btn-primary pull-right">Back</a></h3>
		</div>
	</div>
	<hr>

	<div class="row">
		<div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
			<section class="panel">	
			<div class="panel-heading">Profile Pic</div>
				<div class="panel-body"> 
					<img src="<?php echo !empty($result['profile_image']) ? $result['profile_image'] : base_url('images/default.jpg'); ?>" width="250px" height="250px">													
				</div>
			</section>		
		</div>


		<div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
			<section class="panel">	
			<div class="panel-heading">Farmer Info</div>
				<div class="panel-body"> 
					<div class="table-container">								
						<table class="table custom-table">                    												
							<tr><td>Farmer Id# </td><td>: </td><td><?php echo $result['id']; ?></td></tr>
							<tr><td>Farmer Name</td><td>: </td><td><?php echo $result['user_name']; ?></td></tr>
							<tr><td>Mobile </td><td>: </td><td><?php echo $result['mobile_no']; ?></td></tr>
							<tr><td>Member Since</td><td>: </td><td><?php echo $result['member_since_date']; ?></td></tr>
							<tr><td>Address</td><td>: </td><td><?php echo $result['address']; ?></td></tr>	
						</table>
					</div>									
				</div>
			</section>		
		</div>
	</div>

    </div>
</div>