<div class="row">
    <div class="col-lg-12">

    <div class="row">
		<div class="col-md-8">
			<h3><?php echo $heading; ?></h3>
		</div>
		<div class="col-md-4">
			<h3><a href="javascript:window.history.go(-1);" onclick="loaderPlay();" class="btn btn-primary pull-right">Back</a></h3>
		</div>
	</div>
	<hr>

	<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
		<section class="panel">	
		<div class="panel-heading">Provider Info</div>
			<div class="panel-body"> 
				<div class="table-container">								
					<table class="table custom-table">                    												
                        <tr><td>Ads Id# </td><td>: </td><td><?php echo $result['id']; ?></td></tr>
                        <tr><td>Title</td><td>: </td><td><?php echo $result['title']; ?></td></tr>
                        <tr><td>Machine </td><td>: </td><td><?php echo $result['machine']; ?></td></tr>
                        <tr><td>Machine Type</td><td>: </td><td><?php echo $result['type_of_machine']; ?></td></tr>
                        <tr><td>Description</td><td>: </td><td><?php echo $result['description']; ?></td></tr>
                        <tr><td>Price </td><td>: </td><td><?php echo $result['price']; ?></td></tr>
                        <tr><td>Date</td><td>: </td><td><?php echo $result['date']; ?></td></tr>
                        <tr><td>Provider Details</td><td>: </td>
                            <td>
                                Name: - <?php echo $result['userDetails'][0]['user_name']; ?><br>
                                Mobile: - <?php echo $result['userDetails'][0]['mobile_no']; ?><br>
                                Address: - <?php echo $result['userDetails'][0]['address']; ?><br>
                                Member Since: - <?php echo $result['userDetails'][0]['member_since_date']; ?>
                            </td>
                        </tr>
					</table>
				</div>									
			</div>
		</section>		
	</div>
  
    </div>
</div>