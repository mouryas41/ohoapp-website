<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
	$('#startdate').datepicker({ dateFormat: 'dd-mm-yy' });
	$('#enddate').datepicker({ dateFormat: 'dd-mm-yy' });
	
	$('#myTable').DataTable({
		dom: 'Bfrtip',
        buttons:[
			{ 
				extend: 'excel',
				text: 'Export'
			}
			//'copyHtml5',
            //'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ]
	});	
});

function resetForm(){
    document.getElementById("myForm").reset();
    document.getElementById("startdate").value = '';
    document.getElementById("enddate").value='';	
}
</script>
<div class="row">
    <div class="col-lg-12">
		<h3><?php echo $heading; ?></h3>
		<hr>
		<section class="panel">
			<div class="panel-heading">Search By</div>
			<div class="panel-body">				
				<form class="form-inline" action="<?php echo base_url(); ?>site/machines.html" method="post"
					autocomplete="off" id="myForm">
					<div class="form-group row">
						<label for="startdate" class="col-sm-4 col-form-label">Start Date</label>
						<div class="col-sm-8">
							<input type="text" name="startdate"
								value="<?php echo isset($_POST['startdate']) ? $_POST['startdate']:''; ?>"
								class="form-control" id="startdate" placeholder="dd/mm/yyyy">
						</div>
					</div>
					<div class="form-group row">
						<label for="enddate" class="col-sm-4 col-form-label">End Date</label>
						<div class="col-sm-8">
							<input type="text" name="enddate"
								value="<?php echo isset($_POST['enddate']) ? $_POST['enddate']:''; ?>" class="form-control"
								id="enddate" placeholder="dd/mm/yyyy">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-12">
							<button type="submit" class="btn btn-primary" onclick="loaderPlay();">Filter</button>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-12">
							<button type="button" class="btn btn-danger" onclick="resetForm();">Reset</button>
						</div>
					</div>
				</form>
			</div>
		</section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">		
        <section class="panel">
			<div class="panel-heading">Machines Sales</div>
            <div class="panel-body">
						
                <table class="display table table-responsive table-striped table-advance table-hover" id="myTable">
                    <thead>
                        <tr>
                            <th><i class=""></i> Sr No.</th>
                            <th><i class=""></i>Image</th>
                            <th><i class=""></i>Title</th>
                            <th><i class=""></i>Product Name</th>
                            <th><i class=""></i>Machine Type</th>
                            <th><i class="icon_datareport"></i>Price</th>
                            <th><i class="icon_datareport"></i>Quantity</th>  
							<th><i class="icon_datareport"></i>Total Amount</th>
							<th><i class=""></i>Status</th> 							
                            <th><i class="icon_profile"></i>Customer</th>                            
                        </tr>
                    </thead>
                    <tbody>
                    <?php 					
					if(!empty($results)){
						$i = 1;	
						foreach($results as $rows){ 							
					?>
                       <tr>
							<td><?php echo $i; ?></td>  
							<td><img src="<?php echo $rows['image']; ?>" width="50px" height="50px"></td>							
                            <td><?php echo $rows['title']; ?></td>                            
                            <td><?php echo $rows['machine']; ?></td> 
							<td><?php echo $rows['type']; ?></td> 							
                            <td><?php echo $rows['price']; ?></td>                            
                            <td><?php echo $rows['quentity']; ?></td> 
							<td><?php echo $rows['price'] * $rows['quentity']; ?></td> 
							<td><?php echo $rows['order_status']; ?></td>														
							<td><?php echo $rows['customerId']; ?></td>							
                        </tr>
					<?php 						
						$i++;							
						}
					}					
					?>
					</tbody>
                </table>
            </div>
        </section> 
    </div>
</div>