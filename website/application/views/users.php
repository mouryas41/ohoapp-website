<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
	$('#myTable').DataTable({
		"paging": true,	  
		"searching": true,
		"ordering": true,
		"info": false,
		"lengthChange": false,
		"autoWidth": true,
		
	});
});
</script>

<div class="row">
    <div class="col-lg-12">
        <h3><?php echo $heading; ?></h3><hr>
        <section class="panel">
            <div class="panel-heading">Customers List</div>
            <div class="panel-body">
                <table class="table table-responsive table-striped table-advance table-hover" id="myTable">
                    <thead>
                        <tr>
                            <th><i class=""></i> Sr No.</th>
                            <th><i class="icon_profile"></i>Profile</th>
                            <th><i class=""></i>User Name</th>
                            <th><i class=""></i>email</th>
                            <th><i class=""></i>Mobile No</th>
							<th><i class=""></i>Member Since</th>                            
							<th><i class="icon_clipboard"></i> Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    if(!empty($results)){
                        $i = 1; 
                        //Comparison function 
						function date_compare($element1, $element2){							
							$datetime1 = strtotime(trim($element1['member_since_date'])); 
							$datetime2 = strtotime(trim($element2['member_since_date'])); 
							if($element1['member_since_date']==$element2['member_since_date']) return 0;
							return $element1['member_since_date'] > $element2['member_since_date']?1:-1;
						}  
						//Sort the array in decending order  
                        usort($results, 'date_compare');
                        
                        foreach($results as $user){ 
                    ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td>
                                    <img src="<?php echo !empty($user['profile_image']) ? $user['profile_image'] : base_url('images/default.jpg'); ?>" width="50px" height="50px">
                                </td>                               
                                <td><?php echo $user['user_name']; ?></td>
                                <td><?php echo $user['email']; ?></td>
                                <td><?php echo $user['mobile_no']; ?></td>
                                <td><?php echo $user['member_since_date']; ?></td>                                
                                <td> 
                                    <a href="<?php echo base_url(); ?>users/view/<?php echo $user['user_id']; ?>" class="btn btn-primary btn-sm" onclick="loaderPlay();">View</a>                                   
                                    <!-- <a href="<?php //echo base_url(); ?>users/delete_user/<?php //echo $user['id']; ?>">Delete</a> -->
                                </td>
                            </tr>
                    <?php $i++;
                        } 
                    }
					?>
                </table>
            </div>
        </section>        
    </div>
</div>