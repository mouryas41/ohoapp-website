<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
        <meta name="author" content="GeeksLabs">
        <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
        <link rel="shortcut icon" href="img/favicon.png">

        <title><?php echo $title; ?></title>
        <!-- Bootstrap CSS -->
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
        <!-- bootstrap theme -->
        <link href="<?php echo base_url(); ?>css/bootstrap-theme.css" rel="stylesheet">
        <!--external css-->
        <!-- font icon -->
        <link href="<?php echo base_url(); ?>css/elegant-icons-style.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet" />
        <!-- full calendar css-->
        <link href="<?php echo base_url(); ?>assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
        <!-- easy pie chart-->
        <link href="<?php echo base_url(); ?>assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
        <!-- owl carousel -->
        <link href="<?php echo base_url(); ?>css/owl.carousel.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
        <!-- Custom styles -->
        <link href="<?php echo base_url(); ?>css/fullcalendar.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/widgets.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/style-responsive.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>css/xcharts.min.css" rel=" stylesheet">
        <!--<link href="<?php echo base_url(); ?>css/jquery-ui-1.10.4.min.css" rel="stylesheet">-->
        <link href="<?php echo base_url(); ?>css/jquery.dataTables.min.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>css/buttons.dataTables.min.css" rel="stylesheet" />
        
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
        <!--[if lt IE 9]>
          <script src="<?php echo base_url(); ?>js/html5shiv.js"></script>
          <script src="<?php echo base_url(); ?>js/respond.min.js"></script>
          <script src="<?php echo base_url(); ?>js/lte-ie7.js"></script>
        <![endif]-->
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">        
               
		<script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>js/jquery-ui.min.js"></script>
        <script> 
        //Hide Loading auto       
        document.onreadystatechange = function() { 
            if (document.readyState == "complete") {  
                document.querySelector(".loading").style.display = "none"; 
            } 
        }; 

        //show Loader function 
        function loaderPlay(){
            $('.loading').show();
        }
    </script> 
    </head>
	
    <body>
        <!-- Center Loader -->
		<div class="loading" style="display:none;">Loading&#8230;</div>

        <section id="container"><!-- container section start -->        
            <header class="header dark-bg">
                <div class="toggle-nav">
                    <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"></div>
                </div>
                <!--logo start-->
                <a href="<?php echo base_url(); ?>site" class="logo">Admin <span class="lite">Panel</span></a>
                <!--logo end-->

                <div class="nav search-row" id="top_menu">
                    <!--  search form start -->
                    <ul class="nav top-menu">
                        <li>
                            <!--<form class="navbar-form">
                                <input class="form-control" placeholder="Search" id="search" type="text" name="search">
                            </form>-->
                        </li>
                    </ul>
                    <!--  search form end -->
                </div>
                <div class="top-nav notification-row">
                    <!-- notificatoin dropdown start-->
                    <ul class="nav pull-right top-menu">
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="profile-ava">
                                    <img alt="avatar" src="<?php echo base_url('img/avatar-mini4.jpg'); ?>">
                                </span>
                                <span class="username"><?php echo $this->session->userdata('username'); ?></span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu extended logout">
                                <div class="log-arrow-up"></div>
                                <li class="eborder-top">
                                    <a href="<?php echo base_url(); ?>site/profile.html"><i class="icon_profile"></i>Profile</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>site/logout.html"><i class="icon_key_alt"></i>Log Out</a>
                                </li>
                            </ul>
                        </li>
                        <!-- user login dropdown end -->
                    </ul>
                    <!-- notificatoin dropdown end-->
                </div>
            </header>
        <!--header end-->
        <!--sidebar start-->
        <aside>
            <div id="sidebar"  class="nav-collapse">            
                <!-- sidebar menu start-->
                <ul class="sidebar-menu">
                    <li class="<?php if($title=='Home'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site" onclick="loaderPlay();">
                            <i class="icon_house_alt"></i>
                            <span>Home</span>
                        </a>
                    </li>                    
                    <li class="<?php if($title=='Category'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>category" onclick="loaderPlay();">
                            <i class="icon_clipboard"></i>
                            <span>Category</span>
                        </a>
                    </li>
                    <li class="<?php if($title=='Sub-Category'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>category/subcategory" onclick="loaderPlay();">
                            <i class="icon_clipboard"></i>
                            <span>Sub-Category</span>
                        </a>
                    </li>   
                    <li class="<?php if($title=='Products'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>products" onclick="loaderPlay();">
                            <i class="icon_clipboard"></i>
                            <span>Manage Products</span>
                        </a>
                    </li>                
					<li class="<?php if($title=='Vendors'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site/vendors.html" onclick="loaderPlay();">
                            <i class="icon_clipboard"></i>
                            <span>Vendors</span>
                        </a>
                    </li>

                    <li class="<?php if($title=='Vendor Orders'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>vendor/orders.html" onclick="loaderPlay();">
                            <i class="icon_clipboard"></i>
                            <span>Vendor Orders</span>
                        </a>
                    </li>  
                   					
					<li class="<?php if($title=='Customers'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site/users.html" onclick="loaderPlay();">
                            <i class="icon_id"></i>
                            <span>Customers</span>
                        </a>
                    </li>
                    
                    <li class="<?php if($title=='Customer Orders'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>customer/orders.html" onclick="loaderPlay();">
                            <i class="icon_clipboard"></i>
                            <span>Customer Orders</span>
                        </a>
                    </li>
                    
                										            
                    <li class="<?php if($title=='Advertise'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site/advertise.html" onclick="loaderPlay();">
                            <i class="icon_clipboard"></i>
                            <span>Advertise</span>
                        </a>
                    </li>

                    <li class="<?php if($title=='Sales Report'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site/report.html" onclick="loaderPlay();">
                            <i class="icon_clipboard"></i>
                            <span>Sales Report</span>
                        </a>
                    </li>

                    <!-- <li class="<?php //if($title=='Machines'){ echo 'active';}?>">
                        <a href="<?php //echo base_url(); ?>site/machines.html" onclick="loaderPlay();">
                            <i class="icon_clipboard"></i>
                            <span>Machines Sales</span>
                        </a>
                    </li>-->
                    		


                    <li class="<?php if($title=='Profile'){ echo 'active';}?>">
                        <a href="<?php echo base_url(); ?>site/profile.html" onclick="loaderPlay();">
                            <i class="icon_cog"></i>
                            <span>Settings</span>
                        </a>
                    </li>


					<?php //echo $title; ?>
                </ul>
                <!-- sidebar menu end-->
            </div>
        </aside>
        <!--sidebar end-->

        <!--main content start-->
        <section id="main-content">
            <section class="wrapper">