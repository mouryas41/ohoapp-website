                    </div>
                </div>
                <!-- project team & activity end -->
            </section>
        </section>
    <!--main content end-->
    </section>
    <!-- container section start -->
    <!-- javascripts -->
    <script src="<?php echo base_url(); ?>js/jquery.js"></script>
    <!--<script src="<?php echo base_url(); ?>js/jquery-ui-1.10.4.min.js"></script>-->
    <script src="<?php echo base_url(); ?>js/jquery-1.8.3.min.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>	

	<!-- data table-->		
	<script src="<?php echo base_url(); ?>js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>js/dataTables.buttons.min.js"></script>
	<script src="<?php echo base_url(); ?>js/jszip.min.js"></script>
	<script src="<?php echo base_url(); ?>js/pdfmake.min.js"></script>
	<script src="<?php echo base_url(); ?>js/vfs_fonts.js"></script>
	<script src="<?php echo base_url(); ?>js/buttons.html5.min.js"></script>
	    <!-- bootstrap -->
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="<?php echo base_url(); ?>js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.nicescroll.js" type="text/javascript"></script>

    <!-- custom select -->
    <script src="<?php echo base_url(); ?>js/jquery.customSelect.min.js" ></script>
	
    <!--custome script for all page-->
    <script src="<?php echo base_url(); ?>js/scripts.js"></script>
    <!-- custom script for this page-->

    <script src="<?php echo base_url(); ?>js/jquery.autosize.min.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.placeholder.min.js"></script>
    <script src="<?php echo base_url(); ?>js/gdp-data.js"></script>
    <script src="<?php echo base_url(); ?>js/sparklines.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url(); ?>script/modal.js"></script>
    
    <script>

        //knob
        // $(function() {
            // $(".knob").knob({
                // 'draw' : function () {
                    // $(this.i).val(this.cv + '%')
                // }
            // })
        // });

        //carousel
        // $(document).ready(function() {
            // $("#owl-slider").owlCarousel({
                // navigation : true,
                // slideSpeed : 300,
                // paginationSpeed : 400,
                // singleItem : true

            // });
        // });

        //custom select box

        // $(function(){
            // $('select.styled').customSelect();
        // });

        /* ---------- Map ---------- */
        // $(function(){
            // $('#map').vectorMap({
                // map: 'world_mill_en',
                // series: {
                    // regions: [{
                        // values: gdpData,
                        // scale: ['#000', '#000'],
                        // normalizeFunction: 'polynomial'
                    // }]
                // },
                // backgroundColor: '#eef3f7',
                // onLabelShow: function(e, el, code){
                    // el.html(el.html()+' (GDP - '+gdpData[code]+')');
                // }
            // });
        // });

    </script>

    </body>
</html>
