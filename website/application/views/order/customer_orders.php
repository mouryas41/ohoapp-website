<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
	$('#startdate').datepicker({ dateFormat: 'dd-mm-yy' });
	$('#enddate').datepicker({ dateFormat: 'dd-mm-yy' });
	
	$('#myTable').DataTable({
		dom: 'Bfrtip',
        buttons:[
			{ 
				extend: 'excel',
				text: 'Export'
			}
        ]
	});	
});

function resetForm(){
    document.getElementById("myForm").reset();
    document.getElementById("startdate").value = '';
    document.getElementById("enddate").value='';	
	location.reload(true); 
}
</script>

<div class="row">
    <div class="col-lg-12">
    	<h3><?php echo $heading; ?></h3>
    	<hr>

    	<section class="panel">
			<div class="panel-heading">Search By</div>
    		<div class="panel-body">
    			<form class="form-inline" action="<?php echo base_url(); ?>customer/orders.html" method="post"
    				autocomplete="off" id="myForm">

    				<div class="form-group row">
    					<label for="startdate" class="col-sm-4 col-form-label">Start Date</label>
    					<div class="col-sm-8">
    						<input type="text" name="startdate"
    							value="<?php echo isset($_POST['startdate']) ? $_POST['startdate']:''; ?>"
    							class="form-control" id="startdate" placeholder="dd/mm/yyyy">
    					</div>
    				</div>

    				<div class="form-group row">
    					<label for="enddate" class="col-sm-4 col-form-label">End Date</label>
    					<div class="col-sm-8">
    						<input type="text" name="enddate"
    							value="<?php echo isset($_POST['enddate']) ? $_POST['enddate']:''; ?>"
    							class="form-control" id="enddate" placeholder="dd/mm/yyyy">
    					</div>
    				</div>

    				<div class="form-group row">
    					<div class="col-sm-12">
    						<button type="submit" class="btn btn-primary" onclick="loaderPlay();">Filter</button>
    					</div>
    				</div>

    				<div class="form-group row">
    					<div class="col-sm-12">
    						<button type="button" class="btn btn-danger" onclick="resetForm();">Reset</button>
    					</div>
    				</div>

    			</form>
    		</div>
    	</section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
			<div class="panel-heading">Orders</div>
            <div class="panel-body">						
                <table class="display table" id="myTable">
                    <thead>
                        <tr>
                            <th><i class=""></i> Sr No.</th>
                            <th><i class=""></i>Order ID</th>
							<th><i class=""></i>Order Status</th>                            
                            <th><i class="icon_datareport"></i>Order Date</th>                            
                            <th><i class="icon_datareport"></i>Total Amount</th>                                                                                   
							<th><i class="icon_profile"></i>Customer</th>
                            <th><i class="icon_clipboard"></i> Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 					
					if(!empty($results)){
						$i = 1;	
						foreach($results as $rows){ 
							//if(trim($rows['order_status']) != 'cancelled'){
					?>
                       <tr>
							<td><?php echo $i; ?></td> 							
                            <td><?php echo $rows['order_id']; ?></td>                                                        
							<td><?php echo $rows['order_status']; ?></td> 							
                            <td><?php echo $rows['order_date']; ?></td>                            
                            <td><?php echo $rows['total_amount']; ?></td>                                                                                    						
							<td><?php echo $rows['user_name']; ?></td>	
							<td><a href="<?php echo base_url(); ?>customer/order/view/<?php echo $rows['customerId']; ?>/<?php echo $rows['order_key']; ?>"
									class="btn btn-primary btn-sm" onclick="loaderPlay();">View</a></td>
                        </tr>
                    <?php 
						$i++;
							//}
						}
					}					
					?>
					</tbody>
                </table>
            </div>
        </section> 
    </div>
</div>