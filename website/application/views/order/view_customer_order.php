<div class="row">
	<div class="col-lg-12">
	
	<div class="row">
		<div class="col-md-8">
			<h3><?php echo $heading; ?></h3>
		</div>
		<div class="col-md-4">
			<h3><a href="<?php echo base_url(); ?>customer/orders" onclick="loaderPlay();"
					class="btn btn-primary pull-right">Back</a></h3>
		</div>
	</div>
	<hr>

	<div class="row">
		<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
			<!-- Order Info -->
			<section class="panel">	
				<div class="panel-heading">Order Status</div>
				<div class="panel-body"> 
					<div class="table-container">								
						<table class="table custom-table">                    												
							<tr>
								<td><b>Order ID : </b><?php echo $results['order_id']; ?><br></td>
							</tr>
							<tr>
								<td><b>Order Date : </b><?php echo $results['order_date']; ?>, <?php echo $results['order_time']; ?><br></td>
							</tr>
							<tr>							
								<td><b>Total Items : </b><?php echo count($results['cartItems']); ?><br></td>
							</tr>						
							<tr>
								<td><b>Total Amount : </b><?php echo $results['total_amount']; ?><br></td>
							</tr>
							<tr>
								<td><b>Status : </b><?php echo ucfirst($results['order_status']); ?><br></td>
							</tr>
							<?php if(trim($results['order_status']) == 'delivered'){ ?>
							<tr>
								<td><b>Delivery Date : </b><?php echo $results['order_delivery_date']; ?>, <?php echo $results['order_delivery_time']; ?></td>
							</tr>
							<tr>
								<td><b>Delivered By : </b>
									<?php echo $results['driver_info']['user_name']; ?>
									( <?php echo $results['driver_user_id']; ?> )
								</td>
							</tr>
							<?php } ?>							
						</table>
					</div>									
				</div>
			</section>	

			<!-- Customer Detail -->
			<section class="panel">	
				<div class="panel-heading">Customer Detail</div>
				<div class="panel-body"> 
					<div class="table-container">								
						<table class="table custom-table">                    												
							<tr>
								<td><b>Name : </b><?php echo $results['userDetails']['user_name']; ?><br></td>
							</tr>
							<tr>
								<td><b>Mobile : </b><?php echo $results['userDetails']['mobile_no']; ?><br></td>
							</tr>
							<tr>							
								<td><b>E-mail : </b><?php echo $results['userDetails']['email']; ?><br></td>
							</tr>						
							<tr>
								<td><b>Address : </b><?php echo $results['userDetails']['address']; ?><br></td>
							</tr>
							<tr>
								<td><b>Member Since : </b><?php echo $results['userDetails']['member_since_date']; ?><br></td>
							</tr>						
						</table>
					</div>									
				</div>
			</section>
						
		</div>
	
		<div class="col-lg-8 col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
			
			<section class="panel">	
				<div class="panel-heading">Order Items</div>	 
				<div class="panel-body">
					<div class="table-container">			
						<div class="alert alert-success alert-block" style="display:none;"></div> 
						<div class="table-responsive">                                
							<table class="table custom-table">
								<thead>                            
									<tr>
										<th>Product</th>                               									
										<th>Title</th>									
										<th>Quentity</th>
										<th>Price</th>
										<th>Total</th>                                                                                  
									</tr>
								</thead>
								<tbody id="cartlist">                        
								<?php 
								if(is_array($results['cartItems']) && count($results['cartItems']) > 0){
									foreach($results['cartItems'] as $key => $value){									
										$image = !empty($value['image']) ? $value['image']:''
								?>                             
									<tr>
										<td><img src="<?php echo $image; ?>" width="50px" height="50px"></td>  									                                           
										<td><?php echo $value['title']; ?></td>   									  
										<td><?php echo $value['unit_quantity']; ?></td> 
										<td><?php echo $value['price']; ?></td>  
										<td><?php echo $value['price'] * $value['unit_quantity']; ?></td>                                                                                                                                                                                                                                                 
									</tr>
								<?php 	
									}
								}
								?>     							       
								</tbody>
							</table>
						</div>                
					</div>	
				</div>
			</section>
		</div>
	</div>	

	</div>	
</div>
