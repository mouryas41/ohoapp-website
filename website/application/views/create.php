<!-- Main content -->
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Create User</h3>
        </div>
        <div class="box-body">
            <div class="col-xs-5">
                <?php
                    $exception = $this->session->userdata('exception');
                    if($exception){
                        echo "<div class='alert alert-block alert-danger fade in'><p>";
                        echo $exception;
                        echo "</p></div>";
                        $this->session->unset_userdata('exception');
                    }
                ?>
                <form action="<?php echo base_url(); ?>site/create_user.html" method="post">
                    <div class="form-group has-feedback">
                        <label for="Password">Username</label>
                        <input type="username" name="username" class="form-control" placeholder="Username"/>
                    </div>
					<div class="form-group has-feedback">
                        <label for="Password">Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Password"/>
                    </div>
					<div class="form-group has-feedback">
                        <label for="Password">Role</label>
						<Select name="role" class="form-control">
							<option value="admin">Admin</option>
							<option value="user">User</option>
							<option value="driver">Driver</option>
						</select>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary pull-right">Create</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer"></div>
    </div><!-- /.box -->
</section><!-- /.content -->