<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require __DIR__.'/../../vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class Firebase {

    protected $config	= array();
    protected $serviceAccount;

    public function __construct()
    {
        // Assign the CodeIgniter super-object
		$this->serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/../config/ohoo-114f5-firebase-adminsdk-e24r4-bf723a32fa.json');
    }

    
    public function init()
    {
		$firebase = (new Factory)
							->withServiceAccount($this->serviceAccount)
							->withDatabaseUri('https://ohoo-114f5.firebaseio.com/')
							->create();
		$database = $firebase->getDatabase();
		return $database;
    }
    
}