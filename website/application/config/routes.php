<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "admin";
$route['404_override'] = '';

//Category 
$route['category/add'] = 'category/add_cateogry';
$route['category/edit/(:any)'] = 'category/edit_cateogry/$1';
$route['category/delete'] = 'category/delete_cateogry';

//Sub-Category
$route['category/subcategory/add'] = 'category/add_sub_cateogry';
$route['category/subcategory/edit/(:any)/(:num)'] = 'category/edit_sub_cateogry/$1/$2';
$route['category/subcategory/update'] = 'category/update_sub_cateogry';
$route['category/subcategory/delete'] = 'category/delete_sub_cateogry';

//Vendor orders
$route['vendor/orders'] = 'orders/vendor_orders';
$route['vendor/order/view/(:any)/(:any)'] = 'orders/view_vendor_order/$1/$2';

//Customer orders
$route['customer/orders'] = 'orders/customer_orders';
$route['customer/order/view/(:any)/(:any)'] = 'orders/view_customer_order/$1/$2';

//Products
$route['products'] = 'products/index';
$route['products/import'] = 'products/importdata';

/* End of file routes.php */
/* Location: ./application/config/routes.php */