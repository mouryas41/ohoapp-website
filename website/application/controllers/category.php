<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends CI_Controller{

    function __construct(){

        parent::__construct();
    }


    /* 
	* Category Listing 
	*/
    public function index(){
        $data = array();
        $data["title"] = "Category";
        $data["heading"] = "Manage Category";        
        $data["results"] = $this->category_model->fetch_categories();

        $data["content"] = $this->load->view('category',$data,true);
        $this->load->view('master',$data);
    }


    public function add_cateogry()
    {       
        $this->load->library('form_validation');    
        $this->form_validation->set_rules('category','Category','trim|required');

        if ($this->form_validation->run() == FALSE) {
            $response = array(
                'status' => 'error',
                'message' => validation_errors()
            );
        }
        else
        {            
            $category = $this->input->post('category', true);     
            $newcategory = $this->category_model->add_cateogry($category);

            $results = $this->category_model->fetch_categories();            
            $i = 1; 
            $table = '';
            if(!empty($results)){
                foreach($results as $key => $value){            
                    $table .= '<tr><td>'. $i.'</td><td>'.$key.'</td>';
                    $table .= '<td>'; 
                    $table .= '<a class="btn btn-danger btn-xs" onClick=deleteCategory("'. $key.'");>Delete</a>';                
                    $table .= '</td></tr>';                
                    $i++;
                } 
            }

            $response = array(
                'status' => 'success',
                'message' => "<p>Category created successfully.</p>",
                'table' => $table
            );
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }


    /* 
	* Delete subcategory
	*/
    public function delete_cateogry()
    {       
        $category = $this->input->post('category', true);      
        $deletecategory = $this->category_model->delete_cateogry($category);

        $results = $this->category_model->fetch_categories();            
        $i = 1; 
        $table = '';
        if(!empty($results)){
            foreach($results as $key => $value){            
                $table .= '<tr><td>'. $i.'</td><td>'.$key.'</td>';
                $table .= '<td>'; 
                $table .= '<a class="btn btn-danger btn-xs" onClick=deleteCategory("'. $key.'");>Delete</a>';                
                $table .= '</td></tr>';                
                $i++;
            } 
        }

        $response = array(
            'status' => 'success',
            'message' => "<p>Category Deleted successfully.</p>",
            'table' => $table            
        );  
        $this->output->set_content_type('application/json')->set_output(json_encode($response));      
    }


    function get_file_path_name($a)
    {
        $explode = explode("%2F",$a);
        $part2 = !empty($explode) ? $explode['1']:'';
        $arr = !empty($part2) ? explode("?", $part2):'';
        $filename = !empty($arr) ? $arr['0']:''; 
        return $filename;
    }

    
    /* 
	* Sub-Category Listing 
	*/
    public function subcategory()
    {        
        $data = array();
        $data["title"] = "Sub-Category";
        $data["heading"] = "Manage Sub-Category";     
        $data["categories"] = $this->category_model->fetch_categories();   
        $data["results"] = $this->category_model->fetch_sub_categories();

        $data["content"] = $this->load->view('sub_category',$data,true);
        $this->load->view('master',$data);
    }


    public function add_sub_cateogry()
    {       
        $this->load->library('form_validation');
        $this->form_validation->set_rules('subcategory','Sub Category','trim|xss_clean|required');
        $this->form_validation->set_rules('category','Category','trim|required');

        if ($this->form_validation->run() == FALSE) {
            $response = array(
                'status' => 'error',
                'message' => validation_errors()
            );
        }
        else
        {           
            $title = $this->input->post('subcategory', true);
            $category = $this->input->post('category', true); 
            $image_url = $this->input->post('image_url', true); 
            $image = str_replace("uploads/","uploads%2F",$image_url);
            $new_catid = $this->category_model->check_last_subcategory($category);

            //Add new subcategory 
            $newData = array(
                'id' => $new_catid,      
                'title' => $title,
                'category' => $category,
                'image' => $image,                         
            );
            $newcategory = $this->category_model->add_sub_cateogry($category, $new_catid, $newData);  
                       
            //fetch new subcategory with all records
            $results = $this->category_model->fetch_sub_categories();         
            $i = 1; 
            $table = '';
            if(!empty($results)){
                foreach($results as $key => $value){            
                    $table .= '<tr><td>'. $i.'</td>';
                    
                    if(!empty($value['image'])){
                        $image = $value['image'];
                    }else{
                        $image = 'https://firebasestorage.googleapis.com/v0/b/ohoo-114f5.appspot.com/o/uploads%2Fdefault_category.jpg?alt=media&token=8335f4d2-3886-4a45-bfa2-dd0ca7271e1b';
                    }
                    
                    $table .= '<td><img src="'. $image .'" width="50px" height="50px"></td>';   
                    $table .= '<td>'.$value['title'].'</td>';  						
                    $table .= '<td>'.$value['category'].'</td>';                                                        
                    $table .= '<td>';

                    $filename = !empty($value['image']) ? $this->get_file_path_name($value['image']):'';
                    $filePathName = !empty($filename) ? 'uploads/'.$filename:'';

                    $table .= '<a href="'.base_url('category/subcategory/edit') .'/'. $value['category'] .'/'. $value['id'] .'" class="btn btn-primary btn-xs">Edit</a>|';
                    $table .= '<a onClick=deleteSubcategory("'. $value['category'].'","'.$value['id'].'", "'.$filePathName.'"); class="btn btn-danger btn-xs">Delete</a>';							
                    $table .= '</td></tr>';  
                                  
                    $i++;
                }
            }

            $response = array(
                'status' => 'success',
                'message' => "<p>Subcategory created successfully.</p>",
                'table' => $table
            );            
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }



    public function edit_sub_cateogry($category, $id)
    {        
        $data = array();       
        $data["title"] = "Edit Sub-Category";
        $data["heading"] = "Edit Sub-Category";        
        $data["exist_category"] = $this->category_model->get_subcategory_info($category, $id);
               
        $data["categories"] = $this->category_model->fetch_categories();   
        $data["results"] = $this->category_model->fetch_sub_categories();

        $data["content"] = $this->load->view('sub_category', $data, true);
        $this->load->view('master',$data);
    }


    public function update_sub_cateogry()
    {       
        $this->load->library('form_validation');
        $this->form_validation->set_rules('subcategory','Sub Category','trim|xss_clean|required');
        $this->form_validation->set_rules('category','Category','trim|required');
        //$this->form_validation->set_rules('picture','Image','trim|required');

        if ($this->form_validation->run() == FALSE) {
            $response = array(
                'status' => 'error',
                'message' => validation_errors()
            );
        }
        else
        {
            $title = $this->input->post('subcategory');
            $category = $this->input->post('category'); 
            $subcategory_id = $this->input->post('subcategory_id');             
            $image_url = $this->input->post('image_url'); 
            if(!empty($image_url)){
                $image = str_replace("uploads/","uploads%2F",$image_url);
            }else{
                $image = $this->input->post('old_picture'); 
            }

            $updateData = array(
                'id' => $subcategory_id,      
                'title' => $title,
                'category' => $category,
                'image' => $image,                         
            );
            //$response = $updateData;
            $updatecategory = $this->category_model->update_sub_cateogry($category, $subcategory_id, $updateData);

            $response = array(
                'status' => 'success',
                'message' => "<p>Subcategory updated successfully.</p>"                
            );
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }


    /* 
	* Delete subcategory
	*/
    public function delete_sub_cateogry()
    {       

        $category = $this->input->post('category', true); 
        $sub_catid = $this->input->post('sub_catid', true);        
        $deletecategory = $this->category_model->delete_sub_cateogry($category, $sub_catid);  
               
        //fetch new subcategory with all records
        $results = $this->category_model->fetch_sub_categories();         
        $i = 1; 
        $table = '';
        if(!empty($results)){
            foreach($results as $key => $value){            
                $table .= '<tr><td>'. $i.'</td>';
                    
                if(!empty($value['image'])){
                    $image = $value['image'];
                }else{
                    $image = 'https://firebasestorage.googleapis.com/v0/b/ohoo-114f5.appspot.com/o/uploads%2Fdefault_category.jpg?alt=media&token=8335f4d2-3886-4a45-bfa2-dd0ca7271e1b';
                }
                
                $table .= '<td><img src="'. $image .'" width="50px" height="50px"></td>';   
                $table .= '<td>'.$value['title'].'</td>';  						
                $table .= '<td>'.$value['category'].'</td>';                                                        
                $table .= '<td>';

                $filename = !empty($value['image']) ? $this->get_file_path_name($value['image']):'';
                $filePathName = !empty($filename) ? 'uploads/'.$filename:'';

                $table .= '<a href="'.base_url('category/subcategory/edit') .'/'. $value['category'] .'/'. $value['id'] .'" class="btn btn-primary btn-xs">Edit</a>|';
                $table .= '<a onClick=deleteSubcategory("'. $value['category'].'","'.$value['id'].'", "'.$filePathName.'"); class="btn btn-danger btn-xs">Delete</a>';							
                $table .= '</td></tr>';     

                $i++;
            }
        }

        $response = array(
            'status' => 'success',
            'message' => "<p>Subcategory Deleted successfully.</p>",
            'table' => $table             
        );  

        $this->output->set_content_type('application/json')->set_output(json_encode($response));      
    }


	/* 
	* View Provider details
	*/
	public function view($uid,$advkey){

        $data = array();
        $data['title'] = "View Provider";
        $data['heading'] = "View Provider Details";
        $data['result'] = $this->advertise_model->fetch_by_id($uid,$advkey);
		
        $data['content'] = $this->load->view('view_advertise',$data,true);
        $this->load->view('master',$data);
    }

	/* 
	* Approve advertisement  
	*/
	public function approve($uid,$advkey){

        $data = array();
        $data['title'] = "Approve";
        $data['heading'] = "Approve advertisement";
		$data['result'] = $this->advertise_model->update_status_approve($uid,$advkey);
		redirect('site/advertise');
    }
	
	/* 
	* Reject advertisement  
	*/
	public function reject($uid,$advkey){

        $data = array();
        $data['title'] = "Reject";
        $data['heading'] = "Reject advertisement";
		$data['result'] = $this->advertise_model->update_status_reject($uid,$advkey);
		redirect('site/advertise');
    }
	
	
	

}