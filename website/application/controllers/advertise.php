<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Advertise extends CI_Controller{

    function __construct(){

        parent::__construct();
    }

	/* 
	* View Provider details
	*/
	public function view($uid,$advkey){

        $data = array();
        $data['title'] = "View Provider";
        $data['heading'] = "View Provider Details";
        $data['result'] = $this->advertise_model->fetch_by_id($uid,$advkey);
		
        $data['content'] = $this->load->view('view_advertise',$data,true);
        $this->load->view('master',$data);
    }

	/* 
	* Approve advertisement  
	*/
	public function approve($uid,$advkey){

        $data = array();
        $data['title'] = "Approve";
        $data['heading'] = "Approve advertisement";
		$data['result'] = $this->advertise_model->update_status_approve($uid,$advkey);
		redirect('site/advertise');
    }
	
	/* 
	* Reject advertisement  
	*/
	public function reject($uid,$advkey){

        $data = array();
        $data['title'] = "Reject";
        $data['heading'] = "Reject advertisement";
		$data['result'] = $this->advertise_model->update_status_reject($uid,$advkey);
		redirect('site/advertise');
    }
	
	
	/* 
	* Delete record ---currently not added as functional 
	*/
    public function delete_record($fid){

        $data = $this->advertise_model->delete_record($fid);
        redirect('site/farmers');
    }

}