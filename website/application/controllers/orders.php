<?php
session_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends CI_Controller {

	protected $database;
	
    function __construct(){

        parent::__construct();
		//$this->load->library('firebase');
        $this->database = $this->firebase->init();
        $this->load->model('order_model');
		
        $flag = $this->session->userdata('flag');
        if($flag == NULL){
            redirect('admin','refresh');
        }
    }

    
    /* 
	* Vendor Orders 
	*/
	public function vendor_orders(){
		
        $data = array();
        $data["title"] = "Vendor Orders";
        $data["heading"] = "Vendor Orders";
        $data["base_url"] = base_url() . "site/orders";
		
		$sdate = $this->input->post('startdate');
		$edate = $this->input->post('enddate');
		if(!empty($sdate) && !empty($edate)){ 
            $data['results'] = $this->order_model->fetch_vendor_orders_by_date($sdate, $edate);
		}else{
			$data["results"] = $this->order_model->fetch_vendor_orders();
		}
        
        $data["content"] = $this->load->view('order/vendor_orders',$data,true);		
        $this->load->view('master',$data);		
    }	


    function view_vendor_order($vendorId, $orderId){

        $data = array();
        $data["title"] = "View Order";
        $data["heading"] = "View Order Details";        
        $data["results"] = $this->order_model->view_vendor_order_detail($vendorId, $orderId);
        
        $data["content"] = $this->load->view('order/view_vendor_order',$data, true);		
        $this->load->view('master',$data);		
    }




    /* 
	* Customer Orders 
	*/
	public function customer_orders(){
		
        $data = array();
        $data["title"] = "Customer Orders";
        $data["heading"] = "Customer Orders";
        $data["base_url"] = base_url() . "site/orders";
		
		$sdate = $this->input->post('startdate');
		$edate = $this->input->post('enddate');
		if(!empty($sdate) && !empty($edate)){ 
            $data['results'] = $this->order_model->fetch_customer_orders_by_date($sdate, $edate);
		}else{
			$data["results"] = $this->order_model->fetch_customer_orders();
		}
        
        $data["content"] = $this->load->view('order/customer_orders',$data,true);		
        $this->load->view('master',$data);		
    }	


    function view_customer_order($customerId, $orderId){

        $data = array();
        $data["title"] = "View Order";
        $data["heading"] = "View Order Details";        
        $data["results"] = $this->order_model->view_customer_order_detail($customerId, $orderId);
        
        $data["content"] = $this->load->view('order/view_customer_order',$data, true);		
        $this->load->view('master',$data);		
    }
    
}