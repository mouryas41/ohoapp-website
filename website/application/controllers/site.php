<?php
session_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

	protected $database;
	
    function __construct(){

        parent::__construct();
		//$this->load->library('firebase');
		$this->database = $this->firebase->init();
		
        $flag = $this->session->userdata('flag');
        if($flag == NULL){
            redirect('admin','refresh');
        }
    }

	/* 
	* Logout function 
	*/
    public function logout(){

        $this->session->unset_userdata('flag');
        $this->session->unset_userdata('username');
        $data['message'] = 'You are successfully logged out';
        $this->session->set_userdata($data);
        
        redirect('admin','refresh');
    }


	/* 
	* Home Page 
	*/
    public function index(){

        $data = array();
        $data['title'] = "Home";
        $data['heading'] = "Main Menu";
		$data["mdata"] = $this->sales_model->fetch_machine_sales_data();
		$data["vdata"] = $this->sales_model->fetch_veg_sales_data();
        $data['content'] = $this->load->view('home',$data,true);
        $this->load->view('master',$data);
    }
	
	
	/* 
	* User Profile Page 
	*/
    public function profile(){

        $data = array();
        $data['title'] = "Profile";
        $data['heading'] = "Admin Details";
        $data['content'] = $this->load->view('profile',$data,true);
        $this->load->view('master',$data);
    }


    public function settings(){
        $data = array();
        $data['title'] = "Settings";
        $data['heading'] = "System Management";
        $data['content'] = $this->load->view('settings',$data,true);
        $this->load->view('master',$data);
    }
	
	
	/* 
	* Create user form Page 
	*/
	public function create(){

        $data = array();
        $data['title'] = "Create User";
        $data['heading'] = "Create New User";
        $data['content'] = $this->load->view('create',$data,true);
        $this->load->view('master',$data);
    }

	/* 
	* Create user with validation 
	*/
    public function create_user(){

        $data = array();
        $data['username'] = $this->input->post('username',true);
        $data['password'] = hash("SHA512",$this->input->post('password',true));
        $data['role'] = $this->input->post('role',true);
        $result = $this->admin_model->save_admin($data);

		if($result){
			$data['exception'] = 'Already exist';
            $this->session->set_userdata($data);
			redirect('site/create');
		}else{
			redirect('site');
		}
    }
    
    
	/* 
	* Reset User password form 
	*/
    public function reset(){

        $data = array();
        $data['title'] = "Reset Password";
        $data['heading'] = "Reset Admin Password";
        $data['content'] = $this->load->view('reset',$data,true);
        $this->load->view('master',$data);
    }

	/* 
	* Reset User password function 
	*/
    public function reset_password(){

        $admin_id = $this->session->userdata('flag');
        $password = hash("SHA512",$this->input->post('password',true));
        $this->admin_model->update_password_by_id($admin_id, $password);
        
        redirect('site/profile');
    }
    
    
	/* 
	* Vendors Listing 
	*/
	public function vendors(){

        $data = array();
        $data["title"] = "Vendors";
        $data["heading"] = "Vendors List";
        $data["base_url"] = base_url() . "site/vendors";
        $data["results"] = $this->farmer_model->fetch_records();

        $data["content"] = $this->load->view('vendors',$data,true);
        $this->load->view('master',$data);
    }
	
	/* 
	* Users Listing 
	*/
	public function users(){

        $data = array();
        $data["title"] = "Customers";
        $data["heading"] = "Customer List";
        $data["base_url"] = base_url() . "site/users";
        $data["results"] = $this->user_model->fetch_records();
        
        $data["content"] = $this->load->view('users',$data,true);
        $this->load->view('master',$data);
    }
    
    
	/* 
	* Advertise Listing 
	*/
	public function advertise(){

        $data = array();
        $data["title"] = "Advertise";
        $data["heading"] = "Advertise List";
        $data["base_url"] = base_url() . "site/advertise";
        $data["results"] = $this->advertise_model->fetch_records();
        // echo "<pre>";
        // print_r($data["results"]);
        // echo "</pre>";
        // die;

        $data["content"] = $this->load->view('advertise',$data,true);
        $this->load->view('master',$data);
    }
    
    
	/* 
	* Machines Listing 
	*/
	public function machines(){
		
        $data = array();
        $data["title"] = "Machines";
        $data["heading"] = "Machines Sales";
        $data["base_url"] = base_url() . "site/machines";
		
		$sdate = $this->input->post('startdate');
		$edate = $this->input->post('enddate');
		if(!empty($sdate) && !empty($edate)){ 
			$data['results'] = $this->sales_model->fetch_machine_sales_by_date($sdate,$edate);
		}else{
			$data["results"] = $this->sales_model->fetch_machine_sales_data();
		}
	
        $data["content"] = $this->load->view('machines',$data,true);		
        $this->load->view('master',$data);		
    }
    
    
	/* 
	* Vegetables Listing 
	*/
	public function report(){
		
        $data = array();
        $data["title"] = "Sales Report";
        $data["heading"] = "Sales Report";
        $data["base_url"] = base_url() . "site/report";
		
		$sdate = $this->input->post('startdate');
		$edate = $this->input->post('enddate');
		if(!empty($sdate) && !empty($edate)){ 
			$data['results'] = $this->sales_model->fetch_veg_sales_by_date($sdate,$edate);
		}else{
			$data["results"] = $this->sales_model->fetch_veg_sales_data();
		}
	
        $data["content"] = $this->load->view('report',$data,true);		
        $this->load->view('master',$data);		
    }	        

}