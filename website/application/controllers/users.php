<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

    function __construct(){

        parent::__construct();
    }

    /* 
	* View User details
	*/
	public function view($uid){

        $data = array();
        $data['title'] = "View User";
        $data['heading'] = "View User Details";
        $data['result'] = $this->user_model->fetch_user_by_id($uid);
		
        $data['content'] = $this->load->view('view_user',$data,true);
        $this->load->view('master',$data);
    }

  
}