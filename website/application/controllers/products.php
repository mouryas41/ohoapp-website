<?php
session_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller {

	protected $database;
	
    function __construct(){

        parent::__construct();
		//$this->load->library('firebase');
        $this->database = $this->firebase->init();
        $this->load->model('product_model');
		
        $flag = $this->session->userdata('flag');
        if($flag == NULL){
            redirect('admin','refresh');
        }
    }

    
    /* 
	* Products Listing
	*/
	public function index(){
		
        $data = array();
        $data["title"] = "Products";
        $data["heading"] = "Products";
        $data["base_url"] = base_url() . "products";
		
		// $sdate = $this->input->post('startdate');
		// $edate = $this->input->post('enddate');
		// if(!empty($sdate) && !empty($edate)){ 
        //     $data['results'] = $this->order_model->fetch_vendor_orders_by_date($sdate, $edate);
		// }else{
			$data["results"] = $this->product_model->fetch_products();
		//}
        
        $data["content"] = $this->load->view('product/index',$data,true);		
        $this->load->view('master',$data);		
    }	



   /* 
	* Importexcel 
	*/
    public function importdata()
	{ 
        $data = array();
        $data["title"] = "Import Excel";
        $data["heading"] = "Import Excel";

        $data["vendors"]  = $this->product_model->fetch_vendors();       
        $data["content"] = $this->load->view('product/importExport',$data,true);		                    
        $this->load->view('master',$data);	

		if($this->input->post('submit'))
		{
            $vendor = $this->input->post('vendor');
			$file = $_FILES['file']['tmp_name'];
			$handle = fopen($file, "r");
			$c = 0;//
			while(($filesop = fgetcsv($handle, 1000, ",")) !== false)
			{                
				$category = $filesop[0];
                $title = $filesop[1];
                $quantity = $filesop[2];
                $price = $filesop[3];
				if($c<>0){					//SKIP THE FIRST ROW
					$this->product_model->saverecords($vendor,$category,$title,$quantity,$price);
				}
				$c = $c + 1;
            }            
            $data["message"] = "sucessfully import data !";	
            redirect('products','refresh');      			
        }         
	}




    function view_vendor_order($vendorId, $orderId){

        $data = array();
        $data["title"] = "View Order";
        $data["heading"] = "View Order Details";        
        $data["results"] = $this->order_model->view_vendor_order_detail($vendorId, $orderId);
        
        $data["content"] = $this->load->view('order/view_vendor_order',$data, true);		
        $this->load->view('master',$data);		
    }




    /* 
	* Customer Orders 
	*/
	public function customer_orders(){
		
        $data = array();
        $data["title"] = "Customer Orders";
        $data["heading"] = "Customer Orders";
        $data["base_url"] = base_url() . "site/orders";
		
		$sdate = $this->input->post('startdate');
		$edate = $this->input->post('enddate');
		if(!empty($sdate) && !empty($edate)){ 
            $data['results'] = $this->order_model->fetch_customer_orders_by_date($sdate, $edate);
		}else{
			$data["results"] = $this->order_model->fetch_customer_orders();
		}
        
        $data["content"] = $this->load->view('order/customer_orders',$data,true);		
        $this->load->view('master',$data);		
    }	


    function view_customer_order($customerId, $orderId){

        $data = array();
        $data["title"] = "View Order";
        $data["heading"] = "View Order Details";        
        $data["results"] = $this->order_model->view_customer_order_detail($customerId, $orderId);
        
        $data["content"] = $this->load->view('order/view_customer_order',$data, true);		
        $this->load->view('master',$data);		
    }
    
}