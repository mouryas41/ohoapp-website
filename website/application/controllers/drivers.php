<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Records extends CI_Controller {

    function __construct(){

        parent::__construct();
    }

    public function view($uid){
		
        $data = array();
        $data['title'] = "View User Profile";
        $data['heading'] = "View User Profile Details";
        $data['result'] = $this->user_model->fetch_user_by_id($uid);
        $data['content'] = $this->load->view('view_user',$data,true);
        $this->load->view('master',$data);
    }	
}