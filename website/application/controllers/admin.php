<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	protected $database;
	
    function __construct(){

        parent::__construct();
		//$this->load->library('firebase');
		$this->database = $this->firebase->init();
		
        $flag = $this->session->userdata('flag');
        if($flag != NULL){
            redirect('site','refresh');
        }
    }

	/* 
	* Login form  
	*/
	public function index(){
		
		$this->load->view('login');
	}

	/* 
	* Login authentication  
	*/
    public function authentication(){
		
        $username = $this->input->post('username',true);
        $password = hash("SHA512",$this->input->post('password',true));
        $result = $this->admin_model->login($username,$password);

        if($result){
            $data['flag'] = $result['admin_id'];
            $data['username'] = $result['admin_username'];
            $this->session->set_userdata($data);
            redirect('site');
        }
        else
		{
            $data = array();
            $data['exception'] = 'Your User Id / Password Invalid!';
            $this->session->set_userdata($data);
            redirect('admin');
        }
    }
}