<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendors extends CI_Controller {

    function __construct(){

        parent::__construct();
    }
	
	/* 
	* View farmers details
	*/
	public function view($fid){

        $data = array();
        $data['title'] = "View Vendor";
        $data['heading'] = "View Vendor Details";
        $data['result'] = $this->farmer_model->fetch_by_id($fid);
		
        $data['content'] = $this->load->view('view_farmer',$data,true);
        $this->load->view('master',$data);
    }

	/* 
	* Delete record --- currently not functional 
	*/
    public function delete_record($fid){

        $data = $this->farmer_model->delete_record($fid);
        redirect('site/farmers');
    }	
}