<?php
include('config.php');
include('notification_common.php');

function circle_distance($lat1, $lon1, $lat2, $lon2){
  $rad = M_PI / 180;
  return acos(sin($lat2*$rad) * sin($lat1*$rad) + cos($lat2*$rad) * cos($lat1*$rad) * cos($lon2*$rad - $lon1*$rad)) * 6371;// Kilometers
}
	
$token = array();	
		
$orderData = $database->getReference('OHOUserAppDatabase/orders')->getvalue();

if(count($orderData) > 0){
	
	//Fetch data from ohocrondata table			
	$mdata = $database->getReference('OHOCronData')->getvalue();

	foreach($mdata as $key => $value){
		
		$status =  $value['status'];
		//Check status value, if status equal 0 then send notification 
		if($status == 0){
			
			$order_user_id = $key;
			$farmar_latitude = $value['farmar_latitude'];
			$farmar_longitude =  $value['farmar_longitude'];
			$order_key =  $value['order_key'];
			
			//fetch all record according to order_user_id and order_key based
			$reference = $database->getReference('OHOUserAppDatabase/orders/'.$order_user_id)->getChild($order_key);
			$data = $reference->getvalue(); 
			$order_id = $data['order_id'];
			
			//update status of ohocrondata, set as 1
			if($data['driver_accept_status'] == 1 && $data['driver_user_id'] != 0){
				$database->getReference('OHOCronData/'.$order_user_id)->getChild('status')->set(1);
			}
			//send notification again to all but not this driver, which id saved in driver_user_id
			elseif($data['driver_accept_status'] == 0 && $data['driver_user_id'] != 0){
				
				$driverData = $database->getReference('OHODriverAppDatabase')->getvalue();
				foreach($driverData['users'] as $key => $dvalue){
					$driver_user_id = $key;
					$latitude = $dvalue['latitude'];
					$longitude =  $dvalue['longitude'];
					$deviceToken =  $dvalue['device_token'];
					
					//check distance between two location
					//if less then 20 then send notification
					if(circle_distance($farmar_latitude,$farmar_longitude,$latitude,$longitude) <= 20){
						if($driver_user_id != $data['driver_user_id']){
							$token[] = $deviceToken;
						}
					}
				}
			}else{
			
				//Currently order not accept by any driver, send again to all driver within 20km
				//if($data['driver_accept_status'] == 0 || $data['driver_accept_status'] == 1 && $data['driver_user_id'] == 0){
					
					$driverData = $database->getReference('OHODriverAppDatabase')->getvalue();
					foreach($driverData['users'] as $key => $dvalue){
						print_r($dvalue);
						$latitude = $dvalue['latitude'];
						$longitude =  $dvalue['longitude'];
						$deviceToken =  $dvalue['device_token'];
						
						//check distance between two location
						//if less then 20 then send notification
						if(circle_distance($farmar_latitude,$farmar_longitude,$latitude,$longitude) <= 20 ){
							$token[] = $deviceToken;
						}
					}
				//}
			}
			
			$title = 'Order Received';
			$message = 'New Order received with order id: '.$order_id; 
			if(!empty($token)){
				foreach($token as $key){
					$device_token = $key;
					send_notification($device_token, $title, $message,'driver');
				}
				echo json_encode(array('message'=>'Notification Sent', 'status'=>'1'));
			}else{
				echo json_encode(array('message'=>'Not found near by any driver', 'status'=>'0'));
			}
		}
	}
}
?>
