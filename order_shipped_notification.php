<?php
include('config.php');
include('notification_common.php');

$order_user_id = (isset($_POST['order_user_id'])) ? $_POST['order_user_id'] :'';
$order_key = (isset($_POST['order_key'])) ? $_POST['order_key'] :'';

$ordersRef = $database->getReference('OHOUserAppDatabase/orders/'.$order_user_id)->getChild($order_key);
$orderData = $ordersRef->getvalue();

$user_device_token = $orderData['user_device_token'];
$order_id = $orderData['order_id'];
$cartItems = $orderData['cartItems'];

$product_id = array();
$orderName = array();
if(!empty($cartItems)){
	foreach($cartItems as $key => $ci){
		$move_status = $ci['move_status'];
		$orderName[] = $ci['title'];
		if($move_status != 1){
			$product_id[] = $ci['product_id'];
		}
	}
	
	if(count($orderName) > 2){
    	$cnt = count($orderName)-1;
    	$plus = '+'.$cnt;
    }elseif(count($orderName) == 2){
    	$plus = '+1';
    }else{
    	$plus = '';
    }
    $order_name = $orderName[0].$plus;
    
    //if(empty($product_id)){	
    if(count($product_id) < 1){	    
    	//Send notifcation to user
    	$utitle = "Shipped";
    	$user_message = "Your order:".$order_name."...with OrderID ".$order_id." has been shipped by the seller and moved to deliver.";
    	send_notification($user_device_token, $utitle, $user_message,'driver');
    	
    	//Save notification
		if(trim($order_user_id) != ''){
			$notification = [
				"title" => $utitle,
				"message" => $user_message
			];
			$database->getReference('OHONotification/'.$order_user_id)->push($notification);
		}
		
    	echo json_encode(array('message'=>'Packed item shipped by provider.', 'status'=>'1'));
    }else{
    	echo json_encode(array('message'=>'Some items are pending for collection.', 'status'=>'0'));
    }
    
}
?>