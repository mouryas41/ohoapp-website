<?php
include('config.php');
include('notification_common.php');

$order_user_id = (isset($_POST['order_user_id'])) ? $_POST['order_user_id'] :'';
$order_key = (isset($_POST['order_key'])) ? $_POST['order_key'] :'';

$ordersRef = $database->getReference('OHOUserAppDatabase/orders/'.$order_user_id)->getChild($order_key);
$orderData = $ordersRef->getvalue();

$to_user = $orderData['userDetails']['email'];
$user_device_token = $orderData['user_device_token'];
$order_id = $orderData['order_id'];
$cartItems = $orderData['cartItems'];

$orderName = array();
if(!empty($cartItems)){
	foreach($cartItems as $key => $ci){
		$move_status = $ci['move_status'];
		$orderName[] = $ci['title'];
	}
}

if(count($orderName) > 2){
	$cnt = count($orderName)-1;
	$plus = '+'.$cnt;
}elseif(count($orderName) == 2){
	$plus = '+1';
}else{
	$plus = '';
}
$order_name = $orderName[0].$plus;
$user_message = "Your order:".$order_name."...with OrderID ".$order_id." successfully delivered.";


//ini_set( 'display_errors', 1 );
//error_reporting( E_ALL );
$from = "support@knickglobal.co.in";
$subject = "Order Delivered";
$message = $user_message;
$headers = "From:" . $from;

if(mail($to_user, $subject,$message, $headers)){
    send_notification($user_device_token, $subject, $message,'driver');
    
	//Save notification
	if(trim($order_user_id) != ''){
		$notification = [
			"title" => $subject,
			"message" => $message
		];
		$database->getReference('OHONotification/'.$order_user_id)->push($notification);
	}	
	
	echo json_encode(array('message'=>'The email message was sent.', 'status'=>'1'));
}else{
	echo json_encode(array('message'=>'Some thing went wrong.', 'status'=>'0'));
}
?>