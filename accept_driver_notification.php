<?php
include('config.php');
include('notification_common.php');

$driver_user_id = (isset($_POST['driver_user_id'])) ? $_POST['driver_user_id'] :'';
$order_user_id = (isset($_POST['order_user_id'])) ? $_POST['order_user_id'] :'';
$order_key = (isset($_POST['order_key'])) ? $_POST['order_key'] :'';

$token = array();
$driverRef = $database->getReference('OHODriverAppDatabase/users')->getChild($driver_user_id);
$driverData = $driverRef->getvalue();
$driver_name = $driverData['user_name'];

$ordersRef = $database->getReference('OHOUserAppDatabase/orders/'.$order_user_id)->getChild($order_key);
$orderData = $ordersRef->getvalue();

$user_device_token = $orderData['user_device_token'];
$order_id = $orderData['order_id'];
$cartItems = $orderData['cartItems'];

if(!empty($cartItems)){
	foreach($cartItems as $key => $ci){
		
		//fetch orders data detail from cartitem
		$order_name = $ci['title'];
		$provider_device_token = $ci['user_device_token'];
		$farmer_verification_otp = $ci['farmer_verification_otp'];
		
		//Send notifcation to provider
		$ptitle = "Pick-Up";
		$provider_message = "Your packed item:".$order_name."..pick up accepted by ".$driver_name.". Be ready to move product with verification code ".$farmer_verification_otp;
		send_notification($provider_device_token, $ptitle, $provider_message,'driver');

//change move status as 1		
//$database->getReference('OHOUserAppDatabase/orders/'.$order_user_id.'/'.$order_key.'/cartItems')->getChild($key.'/move_status')->set(1);
		
		//Send notifcation to user
		$utitle = "Shipped";
		$user_message = "Your order:".$order_name."...with OrderID ".$order_id." has been packed and shipped by the seller and delivered soon.";
		send_notification($user_device_token, $utitle, $user_message,'driver');
	}
	echo json_encode(array('message'=>'Packed delivery accepted by driver.', 'status'=>'1'));
}else{
	echo json_encode(array('message'=>'Packed delivery not accepted by any driver', 'status'=>'0'));
}
?>
